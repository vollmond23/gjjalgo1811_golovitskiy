package com.getjavajob.training.algo1811.golovitskiym.lesson04;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

import com.getjavajob.training.algo1811.golovitskiym.util.StopWatch;

import static com.getjavajob.training.algo1811.golovitskiym.util.UsefulStuff.clearFile;

public class DoublyLinkedListPerformanceTest {
    private static final String outputFileName = "src/main/java/com/getjavajob/training/algo1811/golovitskiym/lesson04/outputDoublyLinkedTest.txt";

    public static void main(String[] args) throws IOException {
        clearFile(outputFileName);
        testAddPerformance();
        testRemovePerformance();
    }

    private static void testAddPerformance() throws IOException {
        final int LIST_BIG_SIZE = 25_000_000;
        final int LIST_SMALL_SIZE = 100_000;
        FileWriter fw = new FileWriter(new File(outputFileName), true);
        StopWatch stopWatch = new StopWatch();
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();
        LinkedList<Integer> ll = new LinkedList<>();
        for (int i = 0; i < 3; i++) {
            dll.add(0);
            ll.add(0);
        }
        fw.write("-------- Addition to the beginning --------\n\n");
        stopWatch.start();
        for (int i = 0; i < LIST_BIG_SIZE; i++) {
            dll.add(0, i);
        }
        fw.write("DoublyLinkedList.add(0, e): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < LIST_BIG_SIZE; i++) {
            ll.add(0, i);
        }
        fw.write("LinkedList.add(0, e): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");

        fw.write("-------- Addition to the end --------\n\n");
        stopWatch.start();
        for (int i = 0; i < LIST_BIG_SIZE; i++) {
            dll.add(i);
        }
        fw.write("DoublyLinkedList.add(e): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < LIST_BIG_SIZE; i++) {
            ll.add(i);
        }
        fw.write("LinkedList.add(e): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");
        /*
          Creating new lists (inserting in the middle is pretty hard stuff for linked lists)
         */
        dll = new DoublyLinkedList<>();
        ll = new LinkedList<>();
        for (int i = 0; i < 3; i++) {
            dll.add(0);
            ll.add(0);
        }
        fw.write("-------- Addition to the middle --------\n\n");
        stopWatch.start();
        for (int i = 0; i < LIST_SMALL_SIZE; i++) {
            dll.add(dll. size() / 2, i);
        }
        fw.write("DoublyLinkedList.add(dll.size() / 2, e): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < LIST_SMALL_SIZE; i++) {
            ll.add(ll.size() / 2, i);
        }
        fw.write("LinkedList.add(ll.size() / 2, e): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");
        fw.close();
    }

    private static void testRemovePerformance() throws IOException {
        final int LIST_BIG_SIZE = 120_000_000;
        final int LIST_SMALL_SIZE = 100_000;
        final int REMOVE_BEG_COUNT = 20_000_000;
        final int REMOVE_MID_COUNT = 50_000;
        final int REMOVE_END_COUNT = 50_000_000;
        FileWriter fw = new FileWriter(new File(outputFileName), true);
        StopWatch stopWatch = new StopWatch();
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();
        LinkedList<Integer> ll = new LinkedList<>();
        for (int i = 0; i < LIST_BIG_SIZE; i++) {
            dll.add(0);
            ll.add(0);
        }
        fw.write("-------- Removing from the beginning --------\n\n");
        stopWatch.start();
        for (int i = 0; i < REMOVE_BEG_COUNT; i++) {
            dll.remove(0);
        }
        fw.write("DoublyLinkedList.remove(0): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < REMOVE_BEG_COUNT; i++) {
            ll.remove(0);
        }
        fw.write("LinkedList.remove(0): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");
        fw.write("-------- Removing from the end --------\n\n");
        stopWatch.start();
        for (int i = 0; i < REMOVE_END_COUNT; i++) {
            dll.remove(dll.size() - 1);
        }
        fw.write("DoublyLinkedList.remove(dll.size() - 1): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < REMOVE_END_COUNT; i++) {
            ll.remove(ll.size() - 1);
        }
        fw.write("LinkedList.remove(da.size() - 1): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");
        /*
          Creating lists with smaller size (deleting from the middle is pretty hard stuff for linked lists)
         */
        dll = new DoublyLinkedList<>();
        ll = new LinkedList<>();
        for (int i = 0; i < LIST_SMALL_SIZE; i++) {
            dll.add(0);
            ll.add(0);
        }
        fw.write("-------- Removing from the middle --------\n\n");
        stopWatch.start();
        for (int i = 0; i < REMOVE_MID_COUNT; i++) {
            dll.remove(dll.size() / 2);
        }
        fw.write("DoublyLinkedList.remove(dll.size() / 2): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < REMOVE_MID_COUNT; i++) {
            ll.remove(ll.size() / 2);
        }
        fw.write("LinkedList.remove(ll.size() / 2): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");
        fw.close();
    }
}
