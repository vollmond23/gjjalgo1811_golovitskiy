package com.getjavajob.training.algo1811.golovitskiym.lesson05;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class LinkedListQueueTest {
    public static void main(String[] args) {
        testAdd();
        testRemove();
    }

    private static void testAdd() {
        LinkedListQueue<Integer> llq = new LinkedListQueue<>();
        for (int i = 0; i < 5; i++) {
            llq.add(i);
        }
        assertEquals("Lesson05.LindedListQueue.testAdd", new Object[] {4, 3, 2, 1, 0}, llq.toArray());
    }

    private static void testRemove() {
        LinkedListQueue<Integer> llq = new LinkedListQueue<>();
        for (int i = 0; i < 5; i++) {
            llq.add(i);
        }
        for (int i = 0; i < 3; i++) {
            llq.remove();
        }
        assertEquals("Lesson05.LindedListQueue.testRemove", new Object[] {4, 3}, llq.toArray());
    }
}
