package com.getjavajob.training.algo1811.golovitskiym.lesson08.tree.binary.search.balanced;

import java.util.Comparator;

public class BalanceableTree<E> extends AbstractBalanceableTree<E> {
    public BalanceableTree() {
        super();
    }

    public BalanceableTree(Comparator<E> comparator) {
        super(comparator);
    }
}
