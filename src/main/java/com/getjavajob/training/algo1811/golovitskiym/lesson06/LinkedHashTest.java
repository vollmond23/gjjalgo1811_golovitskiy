package com.getjavajob.training.algo1811.golovitskiym.lesson06;

import com.getjavajob.training.algo1811.golovitskiym.util.StopWatch;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import static com.getjavajob.training.algo1811.golovitskiym.util.UsefulStuff.clearFile;

public class LinkedHashTest {
    private static final String outputFileName = "src/main/java/com/getjavajob/training/algo1811/golovitskiym/lesson06/outputLinkedHashTest.txt";

    public static void main(String[] args) throws IOException {
        clearFile(outputFileName);
        testPerformance();
    }

    private static void testPerformance() throws IOException {
        FileWriter fw = new FileWriter(new File(outputFileName), true);
        StopWatch stopWatch = new StopWatch();
        LinkedHashMap<Integer, String> linkedHashMap = new LinkedHashMap<>();
        LinkedHashSet<String> linkedHashSet = new LinkedHashSet<>();
        fw.write("-------- Add/Put --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 15_000_000; i++) {
            linkedHashMap.put(i, "string" + i);
        }
        fw.write("LinkedHashMap.testPut: " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 15_000_000; i++) {
            linkedHashSet.add("string" + i);
        }
        fw.write("LinkedHashSet.testAdd: " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");
        fw.close();
    }
}
