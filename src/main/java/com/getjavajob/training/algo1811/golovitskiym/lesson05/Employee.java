package com.getjavajob.training.algo1811.golovitskiym.lesson05;

class Employee {
    private String lastName;
    private String firstName;
    private int salary;

    Employee(String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.salary = 0;
    }

    Employee(String lastName, String firstName, int salary) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.salary = salary;
    }

    String getLastName() {
        return lastName;
    }

    void setLastName(String lastName) {
        this.lastName = lastName;
    }

    String getFirstName() {
        return firstName;
    }

    void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    int getSalary() {
        return salary;
    }

    void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", salary=" + salary +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (salary != employee.salary) return false;
        if (lastName != null ? !lastName.equals(employee.lastName) : employee.lastName != null) return false;
        return firstName != null ? firstName.equals(employee.firstName) : employee.firstName == null;
    }

    @Override
    public int hashCode() {
        int result = lastName != null ? lastName.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + salary;
        return result;
    }
}
