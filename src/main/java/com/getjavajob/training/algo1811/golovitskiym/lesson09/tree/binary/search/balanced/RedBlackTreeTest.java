package com.getjavajob.training.algo1811.golovitskiym.lesson09.tree.binary.search.balanced;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class RedBlackTreeTest {
    public static void main(String[] args) {
        testAdd();
        testRemoveCase1();
        testRemoveCase2();
        testRemoveCase3();
    }

    private static void testAdd() {
        RedBlackTree<Integer> rbt = new RedBlackTree<>();
        rbt.add(1);
        rbt.add(2);
        rbt.add(3);
        rbt.add(4);
        rbt.add(5);
        rbt.add(6);
        String expected = "(2B(1B,4R(3B,5B(null,6R))))";
        String actual = rbt.toString();
        assertEquals("Lesson09.testAdd", expected, actual);
    }

    private static void testRemoveCase1() {
        RedBlackTree<Integer> rbt = new RedBlackTree<>();
        rbt.add(1);
        rbt.add(2);
        rbt.add(3);
        rbt.add(4);
        rbt.add(5);
        rbt.add(6);
        rbt.remove(1);
        String expected = "(4B(2B(null,3R),5B(null,6R)))";
        String actual = rbt.toString();
        assertEquals("Lesson09.testRemoveCase1 (no children) ", expected, actual);
    }

    private static void testRemoveCase2() {
        RedBlackTree<Integer> rbt = new RedBlackTree<>();
        rbt.add(1);
        rbt.add(2);
        rbt.add(3);
        rbt.add(4);
        rbt.add(5);
        rbt.add(6);
        rbt.remove(5);
        String expected = "(2B(1B,4R(3B,6B)))";
        String actual = rbt.toString();
        assertEquals("Lesson09.testRemoveCase2 (one child) ", expected, actual);
    }

    private static void testRemoveCase3() {
        RedBlackTree<Integer> rbt = new RedBlackTree<>();
        rbt.add(1);
        rbt.add(2);
        rbt.add(3);
        rbt.add(4);
        rbt.add(5);
        rbt.add(6);
        rbt.remove(4);
        String expected = "(2B(1B,5R(3B,6B))))";
        String actual = rbt.toString();
        assertEquals("Lesson09.testRemoveCase3 (two children) ", expected, actual);
    }
}
