package com.getjavajob.training.algo1811.golovitskiym.lesson04;

import java.util.List;
import java.util.ListIterator;

public class DoublyLinkedList<V> extends AbstractList<V> implements List<V> {
    private int size;
    private Element<V> first;
    private Element<V> last;

    public DoublyLinkedList() {
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean add(V v) {
        if (size == 0) {
            first = last = new Element<>(null, null, v);
        } else {
            Element<V> element = last;
            last = new Element<>(element, null, v);
            element.next = last;
        }
        size++;
        return true;
    }

    @Override
    public void add(int index, V v) {
        if (size == 0 && index == 0) {
            add(v);
        } else {
            checkIndex(index);
            Element<V> element;
            if (index == 0) {
                first = new Element<>(null, first, v);
            } else {
                element = first;
                for (int i = 1; i < index; i++) {
                    element = element.next;
                }
                Element<V> newElement = new Element<>(element, element.next, v);
                element.next = element.next.prev = newElement;
            }
            size++;
        }
    }

    @Override
    public boolean remove(Object o) {
        if (o != null) {
            for (Element<V> element = first; element != null; element = element.next) {
                if (o.equals(element.val)) {
                    element.prev.next = element.next;
                    element.next.prev = element.prev;
                    size--;
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public V remove(int index) {
        checkIndex(index);
        V returnValue;
        if (index == 0) {
            returnValue = first.val;
            first = first.next;
            first.prev = null;
        } else if (index == size - 1) {
            returnValue = last.val;
            last = last.prev;
            last.next = null;
        } else {
            Element<V> element = first;
            for (int i = 0; i < index; i++) {
                element = element.next;
            }
            returnValue = element.val;
            element.prev.next = element.next;
            element.next.prev = element.prev;
        }
        size--;
        return returnValue;
    }

    @Override
    public V get(int index) {
        checkIndex(index);
        Element<V> element = first;
        for (int i = 0; i <= index; i++) {
            element = element.next;
        }
        return element.val;
    }

    @Override
    public V set(int index, V v) {
        checkIndex(index);
        Element<V> element = first;
        for (int i = 0; i <= index; i++) {
            element = element.next;
        }
        V returningValue = element.val;
        element.val = v;
        return returningValue;
    }

    @Override
    public int indexOf(Object o) {
        int index = 0;
        if (o != null) {
            for (Element<V> element = first; element != null; element = element.next) {
                if (!o.equals(element.val)) {
                    index++;
                } else {
                    break;
                }
            }
        }
        if (index == size) {
            return -1;
        }
        return index;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    @Override
    public Object[] toArray() {
        Object[] objects = new Object[size];
        int i = 0;
        for (Element<V> element = first; element != null; element = element.next) {
            objects[i++] = element.val;
        }
        return objects;
    }

    @Override
    public ListIterator<V> listIterator() {
        return null;
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
    }

    private static class Element<V> {
        Element<V> prev;
        Element<V> next;
        V val;

        Element(Element<V> prev, Element<V> next, V val) {
            this.prev = prev;
            this.next = next;
            this.val = val;
        }
    }
}
