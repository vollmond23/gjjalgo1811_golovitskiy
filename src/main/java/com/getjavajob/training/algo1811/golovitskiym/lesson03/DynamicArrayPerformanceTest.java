package com.getjavajob.training.algo1811.golovitskiym.lesson03;

import com.getjavajob.training.algo1811.golovitskiym.util.StopWatch;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class DynamicArrayPerformanceTest {
    private static final String outputFileName = "src/main/java/com/getjavajob/training/algo1811/golovitskiym/lesson03/output.txt";

    public static void main(String[] args) throws IOException {
        testAddPerformance();
        testRemoveBeginingMiddlePerformance();
        testRemoveEndPerformance();
    }

    private static void testAddPerformance() throws IOException {
        FileWriter fw = new FileWriter(new File(outputFileName), true);
        StopWatch stopWatch = new StopWatch();
        DynamicArray da = new DynamicArray(3);
        ArrayList<Object> al = new ArrayList<>(3);
        for (int i = 0; i < 3; i++) {
            da.add(0);
            al.add(0);
        }

        fw.write("-------- Addition to the beginning --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 200_000; i++) {
            da.add(0, i);
        }
        fw.write("DynamicArray.add(0, e): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 200_000; i++) {
            al.add(0, i);
        }
        fw.write("ArrayList.add(0, e): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");

        fw.write("-------- Addition to the middle --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 200_000; i++) {
            da.add(1, i);
        }
        fw.write("DynamicArray.add(5, e): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 200_000; i++) {
            al.add(1, i);
        }
        fw.write("ArrayList.add(5, e): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");

        fw.write("-------- Addition to the end --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 50_000_000; i++) {
            da.add(i);
        }
        fw.write("DynamicArray.add(e): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 50_000_000; i++) {
            al.add(i);
        }
        fw.write("ArrayList.add(e): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");
        fw.close();
    }

    private static void testRemoveBeginingMiddlePerformance() throws IOException {
        FileWriter fw = new FileWriter(new File(outputFileName), true);
        StopWatch stopWatch = new StopWatch();
        DynamicArray da = new DynamicArray(1_000_000);
        ArrayList<Object> al = new ArrayList<>(1_000_000);
        for (int i = 0; i < 1_000_000; i++) {
            da.add(0);
            al.add(0);
        }

        fw.write("-------- Removing from the beginning --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 100_000; i++) {
            da.remove(0);
        }
        fw.write("DynamicArray.remove(0): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 100_000; i++) {
            al.remove(0);
        }
        fw.write("ArrayList.remove(0): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");

        fw.write("-------- Removing from the middle --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 100_000; i++) {
            da.remove(5);
        }
        fw.write("DynamicArray.remove(5): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 100_000; i++) {
            al.remove(5);
        }
        fw.write("ArrayList.remove(5): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");
        fw.close();
    }

    private static void testRemoveEndPerformance() throws IOException {
        FileWriter fw = new FileWriter(new File(outputFileName), true);
        StopWatch stopWatch = new StopWatch();
        DynamicArray da = new DynamicArray(1_000_000_000);
        ArrayList<Object> al = new ArrayList<>(1_000_000_000);
        for (int i = 0; i < 1_000_000_000; i++) {
            da.add(0);
            al.add(0);
        }
        fw.write("-------- Removing from the end --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 1_000_000_000; i++) {
            da.remove(da.size() - 1);
        }
        fw.write("DynamicArray.remove(da.size() - 1): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 1_000_000_000; i++) {
            al.remove(al.size() - 1);
        }
        fw.write("ArrayList.remove(da.size() - 1): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");
        fw.close();
    }
}
