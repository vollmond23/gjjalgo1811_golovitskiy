package com.getjavajob.training.algo1811.golovitskiym.lesson06;

import java.util.*;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class SetTest {
    private static Set<String> testSet = new HashSet<>();

    static {
        testSet.add("string0");
        testSet.add("string1");
        testSet.add("string2");
    }

    public static void main(String[] args) {
        testSize();
        testIsEmpty();
        testContains();
        testIterator();
        testToArray();
        testToArraySpecified();
        testAdd();
        testRemove();
        testContainsAll();
        testAddAll();
        testRetainAll();
        testRemoveAll();
        testClear();
    }

    private static void testSize() {
        Set<String> set = new HashSet<>(testSet);
        assertEquals("Lesson06.Set.testSize", 3, set.size());
    }

    private static void testIsEmpty() {
        Set<String> set = new HashSet<>(testSet);
        assertEquals("Lesson06.Set.testIsEmpty", false, set.isEmpty());
    }

    private static void testContains() {
        Set<String> set = new HashSet<>(testSet);
        assertEquals("Lesson06.Set.testContains", true, set.contains("string1"));
    }

    private static void testIterator() {
        Set<String> set = new HashSet<>(testSet);
        boolean iterable = false;
        for (Class c : set.iterator().getClass().getInterfaces()) {
            if (c.equals(Iterator.class)) {
                iterable = true;
            }
        }
        assertEquals("Lesson06.Set.testIterator", true, iterable);
    }

    private static void testToArray() {
        Set<String> set = new HashSet<>(testSet);
        Object[] strings = set.toArray();
        boolean result = set.contains(String.valueOf(strings[0]))
                && set.contains(String.valueOf(strings[1]))
                && set.contains(String.valueOf(strings[2]));
        assertEquals("Lesson06.Set.testToArray", true, result);
    }

    private static void testToArraySpecified() {
        Set<String> set = new HashSet<>(testSet);
        String[] specifiedArray = new String[set.size()];
        String[] strings = set.toArray(specifiedArray);
        boolean result = set.contains(strings[0])
                && set.contains(strings[1])
                && set.contains(strings[2]);
        assertEquals("Lesson06.Set.testToArraySpecified", true, result);
    }

    private static void testAdd() {
        Set<String> set = new HashSet<>(testSet);
        set.add("string23");
        set.add("string24");
        assertEquals("Lesson06.Set.testAdd", true, set.contains("string23"));
    }

    private static void testRemove() {
        Set<String> set = new HashSet<>(testSet);
        set.remove("string0");
        assertEquals("Lesson06.Set.testRemove", false, set.contains("string0"));
    }

    private static void testContainsAll() {
        Set<String> set = new HashSet<>();
        set.add("string0");
        set.add("string1");
        set.add("string2");
        set.add("string3");
        set.add("string4");
        assertEquals("Lesson06.Set.testContainsAll", true, set.containsAll(testSet));
    }

    private static void testAddAll() {
        Set<String> set = new HashSet<>(testSet);
        set.addAll(testSet);
        System.out.println(set.toString());
        assertEquals("Lesson06.Set.testAddAll", true, set.containsAll(testSet));
    }

    private static void testRetainAll() {
        Set<String> set = new HashSet<>();
        set.add("string0");
        set.add("string1");
        set.add("string2");
        set.add("string3");
        set.add("string4");
        set.retainAll(testSet);
        assertEquals("Lesson06.Set.testContainsAll", false, set.contains("string3") && set.contains("string4"));
    }

    private static void testRemoveAll() {
        Set<String> set = new HashSet<>();
        set.add("string0");
        set.add("string1");
        set.add("string2");
        set.add("string3");
        set.add("string4");
        set.removeAll(testSet);
        assertEquals("Lesson06.Set.testRemoveAll", true, set.contains("string3") && set.contains("string4"));
    }

    private static void testClear() {
        Set<String> set = new HashSet<>(testSet);
        set.clear();
        assertEquals("Lesson06.Set.testClear", true, set.isEmpty());
    }
}
