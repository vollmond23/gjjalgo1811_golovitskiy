package com.getjavajob.training.algo1811.golovitskiym.lesson09;

import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class SortedSetTest {
    public static void main(String[] args) {
        testComparator();
        testSubSet();
        testHeadSet();
        testTailSet();
        testFirst();
        testLast();
    }

    private static void testComparator() {
        SortedSet<Integer> ss = fillUpTreeSet(1, 2, 3, 4, 5);
        assertEquals("Lesson09.testComparator", true, ss.comparator() == null);
    }

    private static void testSubSet() {
        SortedSet<Integer> ss = fillUpTreeSet(1, 2, 3, 4, 5);
        Set<Integer> expectedSet = fillUpTreeSet(2, 3);
        assertEquals("Lesson09.testSubSet", expectedSet.toString(), ss.subSet(2, 4).toString());
    }

    private static void testHeadSet() {
        SortedSet<Integer> ss = fillUpTreeSet(1, 2, 3, 4, 5);
        Set<Integer> expectedSet = fillUpTreeSet(1, 2, 3);
        assertEquals("Lesson09.testHeadSet", expectedSet.toString(), ss.headSet(4).toString());
    }

    private static void testTailSet() {
        SortedSet<Integer> ss = fillUpTreeSet(1, 2, 3, 4, 5);
        Set<Integer> expectedSet = fillUpTreeSet(3, 4, 5);
        assertEquals("Lesson09.testTailSet", expectedSet.toString(), ss.tailSet(3).toString());
    }

    private static void testFirst() {
        SortedSet<Integer> ss = fillUpTreeSet(1, 2, 3, 4, 5);
        assertEquals("Lesson09.testFirst", 1, ss.first());
    }

    private static void testLast() {
        SortedSet<Integer> ss = fillUpTreeSet(1, 2, 3, 4, 5);
        assertEquals("Lesson09.testLast", 5, ss.last());
    }

    @SafeVarargs
    private static <E> TreeSet<E> fillUpTreeSet(E... args) {
        TreeSet<E> treeSet = new TreeSet<>();
        Collections.addAll(treeSet, args);
        return treeSet;
    }
}
