package com.getjavajob.training.algo1811.golovitskiym.lesson07.tree.binary;

import com.getjavajob.training.algo1811.golovitskiym.lesson07.tree.Node;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class LinkedBinaryTreeTest {
    public static void main(String[] args) {
        testAddRoot();
        testPreOrder();
        testPostOrder();
        testBreadthFirst();
        testAdd();
        testAddLeft();
        testAddRight();
        testSet();
        testRemove();
        testLeft();
        testRight();
        testRoot();
        testParent();
        testSize();
        testIterator();
        testNodes();
    }

    private static void testAddRoot() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        lbt.addRoot(23);
        assertEquals("Lesson07.testAddRoot", 23, lbt.root().getElement());
    }

    private static void testAdd() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        lbt.addRoot(23);
        lbt.add(lbt.root(), 32);
        Collection<Node<Integer>> collection = lbt.breadthFirst();
        String expected = "[23, 32]";
        String actual = Arrays.toString(collection.toArray());
        assertEquals("Lesson07.testAdd", expected, actual);
    }

    private static void testAddLeft() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        lbt.addRoot(23);
        lbt.addLeft(lbt.root(), 32);
        Collection<Node<Integer>> collection = lbt.postOrder();
        String expected = "[32, 23]";
        String actual = Arrays.toString(collection.toArray());
        assertEquals("Lesson07.testAddLeft", expected, actual);
    }

    private static void testAddRight() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        lbt.addRoot(23);
        lbt.addLeft(lbt.root(), 32);
        lbt.addRight(lbt.root(), 33);
        Collection<Node<Integer>> collection = lbt.postOrder();
        String expected = "[32, 33, 23]";
        String actual = Arrays.toString(collection.toArray());
        assertEquals("Lesson07.testAddRight", expected, actual);
    }

    private static void testSet() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        lbt.addRoot(23);
        lbt.set(lbt.root(), 32);
        assertEquals("Lesson07.testSet", 32, lbt.root().getElement());
    }

    private static void testRemove() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        lbt.addRoot(23);
        boolean predicate = lbt.remove(lbt.root()) == 23 && lbt.size() == 0;
        assertEquals("Lesson07.testRemove", true, predicate);
    }

    private static void testLeft() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        lbt.addRoot(23);
        lbt.addLeft(lbt.root(), 32);
        assertEquals("Lesson07.testLeft", 32, lbt.left(lbt.root()).getElement());
    }

    private static void testRight() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        lbt.addRoot(23);
        lbt.addRight(lbt.root(), 32);
        assertEquals("Lesson07.testRight", 32, lbt.right(lbt.root()).getElement());
    }

    private static void testRoot() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        lbt.addRoot(23);
        assertEquals("Lesson07.testRoot", 23, lbt.root().getElement());
    }

    private static void testParent() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        lbt.addRoot(23);
        Node<Integer> node = lbt.addRight(lbt.addLeft(lbt.root(), 32), 1);
        assertEquals("Lesson07.testParent", 32, lbt.parent(node).getElement());
    }

    private static void testSize() {
        LinkedBinaryTree<Integer> lbt = createLinkedBinaryTree();
        assertEquals("Lesson07.testSize", 7, lbt.size());
    }

    private static void testIterator() {
        LinkedBinaryTree<Integer> lbt = createLinkedBinaryTree();
        assertEquals("Lesson07.testIterator", true, lbt.iterator() instanceof Iterator);
    }

    private static void testNodes() {
        LinkedBinaryTree<Integer> lbt = createLinkedBinaryTree();
        Collection<Node<Integer>> collection = lbt.nodes();
        String expected = "[3, 1, 4, 0, 5, 2, 6]";
        String actual = Arrays.toString(collection.toArray());
        assertEquals("Lesson07.testNodes", expected, actual);
    }

    private static void testPreOrder() {
        LinkedBinaryTree<Integer> lbt = createLinkedBinaryTree();
        Collection<Node<Integer>> collection = lbt.preOrder();
        String expected = "[0, 1, 3, 4, 2, 5, 6]";
        String actual = Arrays.toString(collection.toArray());
        assertEquals("Lesson07.testPreOrder", expected, actual);
    }

    private static void testPostOrder() {
        LinkedBinaryTree<Integer> lbt = createLinkedBinaryTree();
        Collection<Node<Integer>> collection = lbt.postOrder();
        String expected = "[3, 4, 1, 5, 6, 2, 0]";
        String actual = Arrays.toString(collection.toArray());
        assertEquals("Lesson07.testPostOrder", expected, actual);
    }

    private static void testBreadthFirst() {
        LinkedBinaryTree<Integer> lbt = createLinkedBinaryTree();
        Collection<Node<Integer>> collection = lbt.breadthFirst();
        String expected = "[0, 1, 2, 3, 4, 5, 6]";
        String actual = Arrays.toString(collection.toArray());
        assertEquals("Lesson07.testBreadthFirst", expected, actual);
    }

    private static LinkedBinaryTree<Integer> createLinkedBinaryTree() {
        LinkedBinaryTree<Integer> lbt = new LinkedBinaryTree<>();
        Node<Integer> root = lbt.addRoot(0);
        Node<Integer> childLeft = lbt.add(root, 1);
        Node<Integer> childRight = lbt.add(root, 2);
        lbt.add(childLeft, 3);
        lbt.add(childLeft, 4);
        lbt.add(childRight, 5);
        lbt.add(childRight, 6);
        return lbt;
    }
}
