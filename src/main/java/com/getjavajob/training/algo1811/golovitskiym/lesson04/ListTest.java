package com.getjavajob.training.algo1811.golovitskiym.lesson04;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.ListIterator;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class ListTest {
    public static void main(String[] args) {
        testAddWithIndex();
        testAddAllWithIndex();
        testGet();
        testIndexOf();
        testLastIndexOf();
        testListIterator();
        testListIteratorWithIndex();
        testRemoveWithIndex();
        testSet();
        testSubList();
    }

    private static void testAddWithIndex() {
        ArrayList<Integer> al = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            al.add(0, i);
        }
        assertEquals("Lesson04.ListTest.testAddWithIndex", new Integer[] {4, 3, 2, 1, 0}, al.toArray());
    }

    private static void testAddAllWithIndex() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        ArrayList<Integer> specifiedArray = new ArrayList<>();
        for (int i = 1; i < 4; i++) {
            specifiedArray.add(i * i);
        }
        al.addAll(1, specifiedArray);
        assertEquals("Lesson04.ListTest.testAddAllWithIndex", new Integer[] {0, 1, 4, 9, 1, 2, 3, 4}, al.toArray());
    }

    private static void testGet() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson04.ListTest.testGet", 2, al.get(2));
    }

    private static void testIndexOf() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 2));
        assertEquals("Lesson04.ListTest.testIndexOf", 2, al.get(2));
    }

    private static void testLastIndexOf() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(3, 0, 3, 0, 3));
        assertEquals("Lesson04.ListTest.testLastIndexOf", 4, al.lastIndexOf(3));
    }

    private static void testListIterator() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        boolean iterable = false;
        for (Class c : al.listIterator().getClass().getInterfaces()) {
            if (c.equals(ListIterator.class)) {
                iterable = true;
            }
        }
        assertEquals("Lesson04.ListTest.testListIterator", true, iterable);
    }

    private static void testListIteratorWithIndex() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        boolean iterable = false;
        for (Class c : al.listIterator(2).getClass().getInterfaces()) {
            if (c.equals(ListIterator.class)) {
                iterable = true;
            }
        }
        assertEquals("Lesson04.ListTest.testListIteratorWithIndex", true, iterable);
    }

    private static void testRemoveWithIndex() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        al.remove(2);
        assertEquals("Lesson04.ListTest.testRemoveWithIndex", new Integer[] {0, 1, 3, 4}, al.toArray());
    }

    private static void testSet() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        al.set(2, 23);
        assertEquals("Lesson04.ListTest.testSet", new Integer[] {0, 1, 23, 3, 4}, al.toArray());
    }

    private static void testSubList() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson04.ListTest.testSubList", new Integer[] {1, 2, 3}, al.subList(1, 4).toArray());
    }
}
