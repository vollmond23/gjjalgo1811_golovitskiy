package com.getjavajob.training.algo1811.golovitskiym.lesson05;

public class LinkedListStack<E> {
    private SinglyLinkedList<E> sll;

    LinkedListStack() {
        sll = new SinglyLinkedList<>();
    }

    void push(E e) {
        sll.add(0, e);
    }

    E pop() {
        return sll.remove(0);
    }

    public Object[] toArray() {
        return sll.asList().toArray();
    }
}
