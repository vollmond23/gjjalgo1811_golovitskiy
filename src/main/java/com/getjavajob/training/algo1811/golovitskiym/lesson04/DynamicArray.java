package com.getjavajob.training.algo1811.golovitskiym.lesson04;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.NoSuchElementException;

public class DynamicArray<T> extends AbstractList<T> implements List<T> {
    private T[] workingArray;
    private int size;
    private transient int modCount = 0;

    public DynamicArray() {
        workingArray = (T[]) new Object[10];
        size = 0;
    }

    public DynamicArray(int size) {
        workingArray = (T[]) new Object[size];
        this.size = 0;
    }

    void expandWorkingArray() {
        workingArray = Arrays.copyOf(workingArray, (int) (workingArray.length * 1.5));
    }

    void checkWorkingArraySize() {
        if (size >= workingArray.length) {
            expandWorkingArray();
        }
    }

    public boolean add(T t) {
        checkWorkingArraySize();
        workingArray[size] = t;
        size++;
        modCount++;
        return true;
    }

    public void add(int i, T t) {
        checkWorkingArraySize();
        checkRange(i);
        System.arraycopy(workingArray, i, workingArray, i + 1, size - i);
        workingArray[i] = t;
        size++;
        modCount++;
    }

    public T set(int i, T e) {
        checkRange(i);
        T returningValue = workingArray[i];
        workingArray[i] = e;
        return returningValue;
    }

    public T get(int i) {
        checkRange(i);
        return workingArray[i];
    }

    public T remove(int i) {
        checkRange(i);
        T returningValue = workingArray[i];
        System.arraycopy(workingArray, i + 1, workingArray, i, size - i - 1);
        workingArray[size - 1] = null;
        size--;
        modCount++;
        return returningValue;
    }

    public boolean remove(Object o) {
        boolean objectWasRemoved = false;
        int index = indexOf(o);
        if (index > -1) {
            remove(index);
            objectWasRemoved = true;
        }
        return objectWasRemoved;
    }

    public int size() {
        return size;
    }

    public int indexOf(Object e) {
        int returningValue = -1;
        for (int i = 0; i < size; i++) {
            if (workingArray[i].equals(e)) {
                returningValue = i;
            }
        }
        return returningValue;
    }

    public boolean contains(Object e) {
        return indexOf(e) > -1;
    }

    public T[] toArray() {
        return Arrays.copyOf(workingArray, size);
    }

    void checkRange(int index) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException("ArrayIndexOutOfBoundsException");
        }
    }

    public DynamicArray.ListIteratorImpl listIterator() {
        return new ListIteratorImpl();
    }

    public class ListIteratorImpl implements java.util.ListIterator<T> {
        private int currentIndex = 0;
        private int lastIndex = -1;
        private int expectedModCount = modCount;

        final void checkForComodification() {
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
        }

        @Override
        public boolean hasNext() {
            return currentIndex < size;
        }

        @Override
        public T next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            lastIndex = currentIndex++;
            return workingArray[lastIndex];
        }

        @Override
        public boolean hasPrevious() {
            return currentIndex > 0;
        }

        @Override
        public T previous() {
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            lastIndex = --currentIndex;
            return workingArray[lastIndex];
        }

        @Override
        public int nextIndex() {
            return currentIndex;
        }

        @Override
        public int previousIndex() {
            return currentIndex - 1;
        }

        @Override
        public void remove() {
            if (lastIndex < 0) {
                throw new IllegalStateException();
            }
            checkForComodification();
            try {
                DynamicArray.this.remove(lastIndex);
                currentIndex = lastIndex;
                lastIndex = -1;
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public void set(T t) {
            if (lastIndex < 0)
                throw new IllegalStateException();
            checkForComodification();
            try {
                DynamicArray.this.set(lastIndex, t);
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public void add(T t) {
            checkForComodification();
            try {
                DynamicArray.this.add(currentIndex, t);
                currentIndex++;
                lastIndex = -1;
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
