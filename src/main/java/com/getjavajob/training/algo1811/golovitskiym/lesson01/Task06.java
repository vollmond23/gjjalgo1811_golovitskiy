package com.getjavajob.training.algo1811.golovitskiym.lesson01;

import static com.getjavajob.training.algo1811.golovitskiym.util.DataFromConsole.getIntegerFromConsole;
import static com.getjavajob.training.algo1811.golovitskiym.util.DataFromConsole.getIntegerFromConsoleInRange;

public class Task06 {
    public static void main(String[] args) {
        System.out.print("Input a number 'n' between 0 and 30: ");
        int n = getIntegerFromConsoleInRange(0, 30);
        System.out.print("Input a number 'm' between 0 and 30: ");
        int m = getIntegerFromConsoleInRange(0, 30);
        System.out.println("a) 2^n = " + subtaskA(n));
        System.out.println("b) 2^n + 2^m = " + subtaskB(n, m));
        System.out.print("Input integer number 'a': ");
        int a = getIntegerFromConsole();
        System.out.print("Input a number 'n' more then 0: ");
        n = getIntegerFromConsoleInRange(1, Integer.MAX_VALUE);
        System.out.println("c) a with n reset lower bits = " + subtaskC(a, n));
        System.out.println("d) a with n'th bit set to 1 = " + subtaskD(a, n));
        System.out.println("e) a with inverted n'th bit = " + subtaskE(a, n));
        System.out.println("f) a with n'th bit set to 0 = " + subtaskF(a, n));
        System.out.println("g) n lower bits of a = " + subtaskG(a, n));
        System.out.println("h) n'th bit of a = " + subtaskH(a, n));
        System.out.print("Input byte number 'a': ");
        byte b = (byte) getIntegerFromConsoleInRange(-128, 127);
        System.out.println("i) byte a as binary number = " + subtaskI(b));
    }

    /**
     * Method calculates 2^n using bit shift only
     *
     * @param n power
     */
    static int subtaskA(int n) {
        return 1 << n;
    }

    /**
     * Method calculates (2^n) + (2^m) using bit shift only
     *
     * @param n power
     * @param m power
     */
    static int subtaskB(int n, int m) {
        return addingWithBitwise(1 << n, 1 << m);
    }

    /**
     * Getting sum of a and b with bitwise operators only
     *
     * @param a first argument
     * @param b second argument
     */
    private static int addingWithBitwise(int a, int b) {
        int carry = a & b;
        int result = a ^ b;
        while(carry != 0)
        {
            int shiftedcarry = carry << 1;
            carry = result & shiftedcarry;
            result ^= shiftedcarry;
        }
        return result;
    }

    /**
     * Reset n lower bits in integer a
     *
     * @param a integer number
     * @param n number of lower bits to reset
     */
    static int subtaskC(int a, int n) {
        int mask = addingWithBitwise(~(1 << n), 1);
        return a & mask;
    }

    /**
     * Set n'th bit of integer number a to 1
     *
     * @param a integer number
     * @param n index of bit to set with 1
     */
    static int subtaskD(int a, int n) {
        int mask = 1 << n - 1;
        return a | mask;
    }

    /**
     * Invert n'th bit of integer number a
     *
     * @param a integer number
     * @param n index of bit to invert
     */
    static int subtaskE(int a, int n) {
        int mask = 1 << n - 1;
        return a ^ mask;
    }

    /**
     * Set n'th bit of integer number a to 0
     *
     * @param a integer number
     * @param n index of bit to set with 0
     */
    static int subtaskF(int a, int n) {
        int mask = ~(1 << n - 1);
        return a & mask;
    }

    /**
     * Return n lower bits in integer a
     *
     * @param a integer number
     * @param n number of lower bits to reset
     */
    static int subtaskG(int a, int n) {
        int mask = addingWithBitwise(~(1 << n), 1);
        return a & ~mask;
    }

    /**
     * Return n'th bit of integer a
     *
     * @param a integer number
     * @param n index of bit to return
     */
    static int subtaskH(int a, int n) {
        int mask = addingWithBitwise(~(1 << n), 1);
        return (a & ~mask) >>> n - 1;
    }

    /**
     * Output bin representation using bit ops (without jdk api)
     *
     * @param a byte number
     */
    static String subtaskI(byte a) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            sb.append(a & 1);
            a >>= 1;
        }
        sb.reverse();
        return sb.toString();
    }
}
