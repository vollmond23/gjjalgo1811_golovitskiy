package com.getjavajob.training.algo1811.golovitskiym.lesson07.tree.binary;

import com.getjavajob.training.algo1811.golovitskiym.lesson07.tree.Node;

import java.util.Collection;
import java.util.Iterator;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {
    public enum Color {RED, BLACK}
    private int size = 0;
    private Node<E> root;

    // nonpublic utility

    protected void setRoot(Node<E> root) {
        this.root = root;
    }

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            return null;
        }
        if (n instanceof NodeImpl) {
            return (NodeImpl<E>) n;
        } else {
            throw new IllegalArgumentException();
        }
    }

    // update methods supported by this class

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (root == null) {
            root = new NodeImpl<>(e);
            size++;
            return root;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> parent = validate(n);
        if (parent.leftChild == null) {
            return addLeft(n, e);
        } else if (parent.rightChild == null) {
            return addRight(n, e);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (node.leftChild == null) {
            node.leftChild = new NodeImpl<>(e);
            size++;
            return node.leftChild;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (node.rightChild == null) {
            node.rightChild = new NodeImpl<>(e);
            size++;
            return node.rightChild;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> parent = validate(n);
        E element = parent.element;
        parent.setElement(e);
        return element;
    }

    /**
     * Remove the {@link Node} <i>n</i> if it has no children
     *
     * @param n node
     * @return element
     * @throws IllegalArgumentException
     */
    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        E element = node.element;
        if (node.rightChild == null && node.leftChild == null) {
            if (n == root) {
                root = null;
            } else {
                NodeImpl<E> parent = validate(parent(n));
                if (n == parent.leftChild) {
                    parent.leftChild = null;
                }
                if (n == parent.rightChild) {
                    parent.rightChild = null;
                }
            }
            size--;
            return element;
        } else {
            throw new IllegalArgumentException("You can't delete Node with children");
        }
    }

    /**
     * Finds the {@link Node} min element in subtree
     *
     * @param n node
     * @return {@link Node}
     * @throws IllegalArgumentException
     */
    private Node<E> searchMin(Node<E> n) {
        Node<E> currentNode = n;
        while (left(currentNode) != null) {
            currentNode = left(currentNode);
        }
        return currentNode;
    }


    // {@link Tree} and {@link BinaryTree} implementations

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> node = validate(p);
        return node != null ? node.leftChild : null;
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> node = validate(p);
        return node != null ? node.rightChild : null;
    }

    @Override
    public Node<E> root() {
        return root;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        if (root.equals(n)) {
            return null;
        }
        return parent(n, root);
    }

    private Node<E> parent(Node<E> nodeToSearch, Node<E> currentNode) throws IllegalArgumentException {
        NodeImpl<E> curNodeImpl = validate(currentNode);
        if (currentNode == null) {
            return null;
        } else if (curNodeImpl.leftChild == nodeToSearch || curNodeImpl.rightChild == nodeToSearch) {
            return currentNode;
        } else {
            Node<E> parentNode = parent(nodeToSearch, curNodeImpl.leftChild);
            if (parentNode == null) {
                parentNode = parent(nodeToSearch, curNodeImpl.rightChild);
            }
            return parentNode;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return new ElementIterator();
    }

    private class ElementIterator implements Iterator<E> {
        private Iterator<Node<E>> it = nodes().iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public E next() {
            return it.next().getElement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public Collection<Node<E>> nodes() {
        return inOrder();
    }

    protected static class NodeImpl<E> implements Node<E> {
        private Color color = Color.RED;
        private E element;
        private Node<E> leftChild;
        private Node<E> rightChild;

        public void setElement(E element) {
            this.element = element;
        }

        public void setLeftChild(Node<E> leftChild) {
            this.leftChild = leftChild;
        }

        public void setRightChild(Node<E> rightChild) {
            this.rightChild = rightChild;
        }

        public void setColor(Color color) {
            this.color = color;
        }

        NodeImpl(E element) {
            this.element = element;
        }

        @Override
        public E getElement() {
            return element;
        }

        @Override
        public String toString() {
            return element.toString();
        }

        public Node<E> getLeftChild() {
            return leftChild;
        }

        public Node<E> getRightChild() {
            return rightChild;
        }

        public Color getColor() {
            return color;
        }
    }
}