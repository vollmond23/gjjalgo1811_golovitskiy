package com.getjavajob.training.algo1811.golovitskiym.lesson09;

import java.io.*;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class CityLookup {
    public static void main(String[] args) {
        testLookupCities();
        testLookupCitiesWithMaxCharacter();
    }

    private static class Cities {
        TreeSet<String> cities = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

        Cities() {
            try {
                File file = new File("src/main/java/com/getjavajob/training/algo1811/golovitskiym/lesson09/cities.txt");
                FileReader fileReader = new FileReader(file);
                BufferedReader reader = new BufferedReader(fileReader);
                String line = reader.readLine();
                while (line != null) {
                    cities.add(line);
                    line = reader.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        TreeSet<String> getCities() {
            return cities;
        }
    }

    private static TreeSet<String> lookupCities(TreeSet<String> cities, String substring) {
        char[] chars = substring.toCharArray();
        char lastSymbolFrom = chars[chars.length - 1];
        char lastSymbolTo = (char) (lastSymbolFrom + 1);
        chars[chars.length - 1] = lastSymbolTo;
        String substringUp = String.valueOf(chars);
        SortedSet<String> citiesSubSet;
        if (lastSymbolFrom > lastSymbolTo) {
            citiesSubSet = cities.subSet(substringUp, substring);
        } else {
            citiesSubSet = cities.subSet(substring, substringUp);
        }
        return (TreeSet<String>) citiesSubSet;
    }

    private static void testLookupCities() {
        Cities cities = new Cities();
        String actualString = lookupCities(cities.getCities(), "an").toString();
        String expectedString = "[Andorra la Vella, Ankara, Antananarivo]";
        assertEquals("Lesson09.testLookupCities", expectedString, actualString);
    }

    private static void testLookupCitiesWithMaxCharacter() {
        Cities cities = new Cities();
        String actualString = lookupCities(cities.getCities(), ("an" + Character.MAX_VALUE)).toString();
        String expectedString = "[Andorra la Vella, Ankara, Antananarivo]";
        assertEquals("Lesson09.testLookupCitiesWithMaxCharacter", expectedString, actualString);
    }
}
