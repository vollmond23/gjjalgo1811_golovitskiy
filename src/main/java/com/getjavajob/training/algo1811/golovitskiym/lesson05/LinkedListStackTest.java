package com.getjavajob.training.algo1811.golovitskiym.lesson05;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class LinkedListStackTest {
    public static void main(String[] args) {
        testPush();
        testPop();
    }

    private static void testPush() {
        LinkedListStack<Integer> llq = new LinkedListStack<>();
        for (int i = 0; i < 5; i++) {
            llq.push(i);
        }
        assertEquals("Lesson05.LindedListStack.testPush", new Object[] {4, 3, 2, 1, 0}, llq.toArray());
    }

    private static void testPop() {
        LinkedListStack<Integer> lls = new LinkedListStack<>();
        for (int i = 0; i < 5; i++) {
            lls.push(i);
        }
        for (int i = 0; i < 3; i++) {
            lls.pop();
        }
        assertEquals("Lesson05.LindedListStack.testPop", new Object[] {1, 0}, lls.toArray());
    }
}
