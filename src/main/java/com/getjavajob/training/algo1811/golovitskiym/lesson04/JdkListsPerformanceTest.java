package com.getjavajob.training.algo1811.golovitskiym.lesson04;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import com.getjavajob.training.algo1811.golovitskiym.util.StopWatch;

import static com.getjavajob.training.algo1811.golovitskiym.util.UsefulStuff.clearFile;

public class JdkListsPerformanceTest {
    private static final String outputFileName = "src/main/java/com/getjavajob/training/algo1811/golovitskiym/lesson04/outputJdkListsTest.txt";

    public static void main(String[] args) throws IOException {
        clearFile(outputFileName);
        testAddPerformance();
        testRemovePerformance();
    }

    private static void testAddPerformance() throws IOException {
        FileWriter fw = new FileWriter(new File(outputFileName), true);
        StopWatch stopWatch = new StopWatch();
        ArrayList<Integer> al = new ArrayList<>();
        LinkedList<Integer> ll = new LinkedList<>();
        for (int i = 0; i < 3; i++) {
            al.add(0);
            ll.add(0);
        }
        fw.write("-------- Addition to the beginning --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 100_000; i++) {
            al.add(0, i);
        }
        fw.write("ArrayList.add(0, e): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 100_000; i++) {
            ll.add(0, i);
        }
        fw.write("LinkedList.add(0, e): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");

        fw.write("-------- Addition to the middle --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 100_000; i++) {
            al.add(al.size() / 2, i);
        }
        fw.write("ArrayList.add(al.size() / 2, e): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 100_000; i++) {
            ll.add(ll.size() / 2, i);
        }
        fw.write("LinkedList.add(ll.size() / 2, e): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");

        fw.write("-------- Addition to the end --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 50_000_000; i++) {
            al.add(i);
        }
        fw.write("ArrayList.add(e): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 50_000_000; i++) {
            ll.add(i);
        }
        fw.write("LinkedList.add(e): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");
        fw.close();
    }

    private static void testRemovePerformance() throws IOException {
        FileWriter fw = new FileWriter(new File(outputFileName), true);
        StopWatch stopWatch = new StopWatch();
        ArrayList<Integer> al = new ArrayList<>();
        LinkedList<Integer> ll = new LinkedList<>();
        for (int i = 0; i < 500_000; i++) {
            al.add(0);
            ll.add(0);
        }
        fw.write("-------- Removing from the beginning --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 100_000; i++) {
            al.remove(0);
        }
        fw.write("ArrayList.remove(0): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 100_000; i++) {
            ll.remove(0);
        }
        fw.write("LinkedList.remove(0): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");

        fw.write("-------- Removing from the middle --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 100_000; i++) {
            al.remove(al.size() / 2);
        }
        fw.write("ArrayList.remove(al.size() / 2): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 100_000; i++) {
            ll.remove(ll.size() / 2);
        }
        fw.write("LinkedList.remove(ll.size() / 2): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");

        fw.write("-------- Removing from the end --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 100_000; i++) {
            al.remove(al.size() - 1);
        }
        fw.write("ArrayList.remove(da.size() - 1): " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 100_000; i++) {
            ll.remove(ll.size() - 1);
        }
        fw.write("LinkedList.remove(da.size() - 1): " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");
        fw.close();
    }
}
