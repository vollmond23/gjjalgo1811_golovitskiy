package com.getjavajob.training.algo1811.golovitskiym.lesson06;

import com.getjavajob.training.algo1811.golovitskiym.util.StopWatch;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import static com.getjavajob.training.algo1811.golovitskiym.util.UsefulStuff.clearFile;
import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class AssociativeArrayTest {
    private static final String outputFileName = "src/main/java/com/getjavajob/training/algo1811/golovitskiym/lesson06/outputAssociativeArrayTest.txt";

    public static void main(String[] args) throws IOException {
        clearFile(outputFileName);
        testPerformance();
        testAddWords();
        testRemove();
    }

    private static void testPerformance() throws IOException {
        FileWriter fw = new FileWriter(new File(outputFileName), true);
        StopWatch stopWatch = new StopWatch();
        AssociativeArray<Integer, String> aa = new AssociativeArray<>();
        HashMap<Integer, String> hm = new HashMap<>();
        fw.write("-------- Addition --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 20_000_000; i++) {
            aa.add(i, Integer.toString(i));
        }
        fw.write("AssociativeArray.testAdd: " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 20_000_000; i++) {
            hm.put(i, Integer.toString(i));
        }
        fw.write("HashMap.testPut: " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");
        fw.write("-------- Getting --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 10_000_000; i++) {
            aa.get(i);
        }
        fw.write("AssociativeArray.testGet: " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 10_000_000; i++) {
            hm.get(i);
        }
        fw.write("HashMap.testGet: " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");
        fw.write("-------- Removing --------\n\n");
        stopWatch.start();
        for (int i = 0; i < 10_000_000; i++) {
            aa.remove(i);
        }
        fw.write("AssociativeArray.testRemove: " + stopWatch.getElapsedTime() + " ms\n");
        stopWatch.start();
        for (int i = 0; i < 10_000_000; i++) {
            hm.remove(i);
        }
        fw.write("HashMap.testRemove: " + stopWatch.getElapsedTime() + " ms\n");
        fw.write("\n");
        fw.close();
    }

    private static void testAddWords() {
        AssociativeArray<String, String> aa = new AssociativeArray<>();
        aa.add("polygenelubricants", "polygenelubricants");
        aa.add("random", "random");
        System.out.println(aa.get("polygenelubricants"));
        System.out.println(aa.get("random"));
    }

    private static void testRemove() {
        AssociativeArray<String, String> aa = new AssociativeArray<>();
        aa.add("key1", "string1");
        assertEquals("Lesson06.testRemove", "string1", aa.remove("key1"));
    }
}
