package com.getjavajob.training.algo1811.golovitskiym.lesson05;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class CollectionUtilsTest {
    public static void main(String[] args) {
        testFilter();
        testTransform();
        testTransformWithNewCollection();
        testForAllDo();
        testUnmodifiableCollection();
    }

    private static void testFilter() {
        List<Employee> employees = new ArrayList<>(Arrays.asList(new Employee("Smith", "Mike"),
                new Employee("Smith", "Jake"),
                new Employee("Black", "Joe")));
        CollectionUtils.filter(employees, new CollectionUtils.Predicate<Employee>() {
            @Override
            public boolean test(Employee object) {
                return object.getLastName().equals("Smith");
            }
        });
        List<Employee> expectedList = new ArrayList<>(Arrays.asList(new Employee("Smith", "Mike"),
                new Employee("Smith", "Jake")));
        assertEquals("Lesson05.testFilter", expectedList.toArray(), employees.toArray());
    }

    private static void testTransform() {
        List<Employee> employees = new ArrayList<>(Arrays.asList( new Employee("Smith", "Mike"),
                new Employee("Smith", "Jake"),
                new Employee("Black", "Joe")));
        CollectionUtils.transform(employees, new CollectionUtils.Transformer<Employee, String>() {
            @Override
            public String transform(Employee input) {
                return input.getLastName();
            }
        });
        String expectedString = "[Smith, Smith, Black]";
        assertEquals("Lesson05.testTransform", expectedString, employees.toString());
    }

    private static void testTransformWithNewCollection() {
        List<Employee> employees = new ArrayList<>(Arrays.asList(new Employee("Smith", "Mike"),
                                        new Employee("Smith", "Jake"),
                                        new Employee("Black", "Joe")));

        List<String> lastNames = new ArrayList<>();
        CollectionUtils.transform(employees, new CollectionUtils.Transformer<Employee, String>() {
            @Override
            public String transform(Employee input) {
                return input.getLastName();
            }
        }, lastNames);
        String expectedString = "[Smith, Smith, Black]";
        assertEquals("Lesson05.testTransformWithNewCollection", expectedString, lastNames.toString());
    }

    private static void testForAllDo() {
        List<Employee> employees = new ArrayList<>(Arrays.asList(new Employee("Smith", "Mike", 1000),
                new Employee("Smith", "Jake", 1500),
                new Employee("Black", "Joe", 900)));
        CollectionUtils.forAllDo(employees, new CollectionUtils.Closure<Employee>() {
            @Override
            public void execute(Employee input) {
                input.setSalary(input.getSalary() + 500);
            }
        });
        List<Integer> salaries = new ArrayList<>();
        CollectionUtils.transform(employees, new CollectionUtils.Transformer<Employee, Integer>() {
            @Override
            public Integer transform(Employee input) {
                return input.getSalary();
            }
        }, salaries);
        String expectedString = "[1500, 2000, 1400]";
        assertEquals("Lesson05.testForAllDo", expectedString, salaries.toString());
    }

    private static void testUnmodifiableCollection() {
        System.out.println("Lesson05.testUnmodifiableCollection:");
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Smith", "Mike"));
        employees.add(new Employee("Smith", "Jake"));
        employees.add(new Employee("Black", "Joe"));
        Collection<Employee> unmodifiableEmployees = CollectionUtils.unmodifiableCollection(employees);
        System.out.println("Size of unmodifiable collection: " + unmodifiableEmployees.size());
        System.out.println("Iterator test, foreach loop: ");
        for (Employee employee : unmodifiableEmployees) {
            System.out.println(employee.getLastName() + " " + employee.getFirstName());
        }
        System.out.println("\"unmodifiableEmployees\" contains all elements from \"employees\": " + unmodifiableEmployees.containsAll(employees));
        System.out.println("\"unmodifiableEmployees\" is empty: " + unmodifiableEmployees.isEmpty());
        System.out.println("\"unmodifiableEmployees\" contains \"Mike Smith\": " + unmodifiableEmployees.contains(new Employee("Smith", "Mike")));
        try {
            unmodifiableEmployees.add(new Employee("Some", "New Guy"));
        } catch (Exception e) {
            System.out.println("Exception for method \"add\": " + e);
        }
        try {
            unmodifiableEmployees.removeAll(employees);
        } catch (Exception e) {
            System.out.println("Exception for method \"removeAll\": " + e);
        }
        try {
            unmodifiableEmployees.addAll(employees);
        } catch (Exception e) {
            System.out.println("Exception for method \"addAll\": " + e);
        }
        try {
            unmodifiableEmployees.clear();
        } catch (Exception e) {
            System.out.println("Exception for method \"clear\": " + e);
        }
        try {
            employees.remove(0);
            unmodifiableEmployees.retainAll(employees);
        } catch (Exception e) {
            System.out.println("Exception for method \"retainAll\": " + e);
        }
    }
}
