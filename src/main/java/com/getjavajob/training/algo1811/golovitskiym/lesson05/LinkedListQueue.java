package com.getjavajob.training.algo1811.golovitskiym.lesson05;

import java.util.Queue;

public class LinkedListQueue<E> extends AbstractQueue<E> implements Queue<E> {
    private Node<E> head;
    private int size;

    LinkedListQueue() {
        size = 0;
        head = null;
    }

    @Override
    public Object[] toArray() {
        Object[] objects = new Object[size];
        int i = 0;
        for (Node<E> node = head; node != null; node = node.next) {
            objects[i++] = node.val;
        }
        return objects;
    }

    @Override
    public boolean add(E e) {
        head = new Node<>(head, e);
        size++;
        return true;
    }

    @Override
    public E remove() {
        E returningValue;
        Node<E> node = head;
        if (size == 0) {
            throw new IndexOutOfBoundsException();
        } else if (size == 1) {
            returningValue = head.val;
            head = null;
        } else {
            while (node.next.next != null) {
                node = node.next;
            }
            returningValue = node.next.val;
            node.next = null;
        }
        size--;
        return returningValue;
    }

    private static class Node<E> {
        Node<E> next;
        E val;

        Node(Node<E> next, E val) {
            this.next = next;
            this.val = val;
        }
    }
}
