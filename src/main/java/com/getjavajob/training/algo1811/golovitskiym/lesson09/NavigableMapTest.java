package com.getjavajob.training.algo1811.golovitskiym.lesson09;

import java.util.*;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class NavigableMapTest {
    public static void main(String[] args) {
        testLowerEntry();
        testLowerKey();
        testFloorEntry();
        testFloorKey();
        testCeilingEntry();
        testCeilingKey();
        testHigherEntry();
        testHigherKey();
        testFirstEntry();
        testLastEntry();
        testPollFirstEntry();
        testPollLastEntry();
        testDescendingMap();
        testNavigableKeySet();
        testDescendingKeySet();
        testSubMap();
        testHeadMap();
        testTailMap();
    }

    private static void testLowerEntry() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        Map.Entry<Integer, String> expectedEntry = nm.lowerEntry(4);
        assertEquals("Lesson09.testLowerEntry", true, expectedEntry.getKey() == 3 && expectedEntry.getValue().equals("three"));
    }

    private static void testLowerKey() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        assertEquals("Lesson09.testLowerKey", 3, nm.lowerKey(4));
    }

    private static void testFloorEntry() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        Map.Entry<Integer, String> expectedEntry = nm.lowerEntry(13);
        assertEquals("Lesson09.testFloorEntry", true, expectedEntry.getKey() == 4 && expectedEntry.getValue().equals("four"));
    }

    private static void testFloorKey() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        assertEquals("Lesson09.testFloorKey", 4, nm.lowerKey(13));
    }

    private static void testCeilingEntry() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        Map.Entry<Integer, String> expectedEntry = nm.ceilingEntry(2);
        assertEquals("Lesson09.testCeilingEntry", true, expectedEntry.getKey() == 2 && expectedEntry.getValue().equals("two"));
    }

    private static void testCeilingKey() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        assertEquals("Lesson09.testCeilingKey", 2, nm.ceilingKey(2));
    }

    private static void testHigherEntry() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        Map.Entry<Integer, String> expectedEntry = nm.higherEntry(2);
        assertEquals("Lesson09.testHigherEntry", true, expectedEntry.getKey() == 3 && expectedEntry.getValue().equals("three"));
    }

    private static void testHigherKey() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        assertEquals("Lesson09.testHigherKey", 3, nm.higherKey(2));
    }

    private static void testFirstEntry() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        Map.Entry<Integer, String> expectedEntry = nm.firstEntry();
        assertEquals("Lesson09.testFirstEntry", true, expectedEntry.getKey() == 0 && expectedEntry.getValue().equals("zero"));
    }

    private static void testLastEntry() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        Map.Entry<Integer, String> expectedEntry = nm.lastEntry();
        assertEquals("Lesson09.testFirstEntry", true, expectedEntry.getKey() == 4 && expectedEntry.getValue().equals("four"));
    }

    private static void testPollFirstEntry() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        Map.Entry<Integer, String> polledEntry = nm.pollFirstEntry();
        assertEquals("Lesson09.testPollFirstEntry", true, polledEntry.getKey() == 0 && polledEntry.getValue().equals("zero") && !nm.containsKey(0));
    }

    private static void testPollLastEntry() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        Map.Entry<Integer, String> polledEntry = nm.pollLastEntry();
        assertEquals("Lesson09.testPollLastEntry", true, polledEntry.getKey() == 4 && polledEntry.getValue().equals("four") && !nm.containsKey(4));
    }

    private static void testDescendingMap() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        String expectedString = "{4=four, 3=three, 2=two, 1=one, 0=zero}";
        assertEquals("Lesson09.testDescendingMap", expectedString, nm.descendingMap().toString());
    }

    private static void testNavigableKeySet() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        String expectedString = "[0, 1, 2, 3, 4]";
        assertEquals("Lesson09.testNavigableKeySet", expectedString, nm.navigableKeySet().toString());
    }

    private static void testDescendingKeySet() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        String expectedString = "[4, 3, 2, 1, 0]";
        assertEquals("Lesson09.testDescendingKeySet", expectedString, nm.descendingKeySet().toString());
    }

    private static void testSubMap() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        String expectedString = "{1=one, 2=two, 3=three}";
        assertEquals("Lesson09.testSubMap", expectedString, nm.subMap(1, 4).toString());
    }

    private static void testHeadMap() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        String expectedString = "{0=zero, 1=one, 2=two}";
        assertEquals("Lesson09.testHeadMap", expectedString, nm.headMap(3).toString());
    }

    private static void testTailMap() {
        NavigableMap<Integer, String> nm = new TreeMap<>();
        nm.put(0, "zero");
        nm.put(1, "one");
        nm.put(2, "two");
        nm.put(3, "three");
        nm.put(4, "four");
        String expectedString = "{2=two, 3=three, 4=four}";
        assertEquals("Lesson09.testTailMap", expectedString, nm.tailMap(2).toString());
    }


}
