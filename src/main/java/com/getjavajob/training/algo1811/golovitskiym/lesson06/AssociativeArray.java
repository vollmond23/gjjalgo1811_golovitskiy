package com.getjavajob.training.algo1811.golovitskiym.lesson06;

public class AssociativeArray<K,V> {
    private int size;
    private int capacity = 16;
    private Node<K,V>[] table;

    @SuppressWarnings({"rawtypes","unchecked"})
    public AssociativeArray() {
        table = new Node[capacity];
        size = 0;
    }

    private void putNodeInTable (Node<K,V> node, Node<K,V>[] table) {
        int index = node.getKey().hashCode() % table.length;
        Node<K,V> newNode = new Node<>(node.getKey(), node.getValue(), null);
        if (table[index] != null) {
            Node<K,V> lastNode = table[index];
            while (lastNode.hasNext()) {
                lastNode = lastNode.getNext();
            }
            lastNode.setNext(newNode);
        } else {
            table[index] = newNode;
        }
    }

    @SuppressWarnings({"rawtypes","unchecked"})
    private void extendTableCapacity() {
        capacity = table.length * 2;
        Node[] newTable = new Node[capacity];
        for (int i = 0; i < table.length; i++) {
            if (table[i] != null) {
                for (; table[i] != null; table[i] = table[i].getNext()) {
                    putNodeInTable(table[i], newTable);
                }
            }
        }
        table = newTable;
    }

    V add(K key, V val) {
        if (size == capacity) {
            extendTableCapacity();
        }
        V oldValue = null;
        int index = key.hashCode() % table.length;
        if (index < 0) {
            index = -index;
        }
        if (table[index] == null) {
            table[index] = new Node<>(key, val, null);
        } else {
            boolean keyFounded = false;
            Node<K,V> node = table[index];
            for (; node != null; node = node.getNext()) {
                if (node.getKey().equals(key)) {
                    oldValue = node.getValue();
                    node.setValue(val);
                    size--;
                    keyFounded = true;
                }
            }
            if (!keyFounded) {
                table[index] = new Node<>(key, val, table[index]);
            }
        }
        size++;
        return oldValue;
    }

    V get(K key) {
        int keyHash = key.hashCode();
        int index = keyHash % table.length;
        if (index < 0) {
            index = -index;
        }
        V returningValue = null;
        Node<K,V> node = table[index];
        for (; node != null; node = node.getNext()) {
            if (node.getHash() == keyHash) {
                if (node.getKey().equals(key)) {
                    returningValue = node.getValue();
                    break;
                }
            }
        }
        return returningValue;
    }

    V remove(K key) {
        int index = key.hashCode() % table.length;
        if (index < 0) {
            index = -index;
        }
        V returningValue = null;
        Node<K,V> node = table[index];
        if (node.getKey().equals(key)) {
            table[index] = node.getNext();
            node.setNext(null);
            returningValue = node.getValue();
        } else {
            for (; node != null; node = node.getNext()) {
                if (node.getNext().getKey().equals(key)) {
                    returningValue = node.getNext().getValue();
                    node.setNext(node.getNext().getNext());
                    break;
                }
            }
        }
        return returningValue;
    }

    public int getSize() {
        return size;
    }
}
