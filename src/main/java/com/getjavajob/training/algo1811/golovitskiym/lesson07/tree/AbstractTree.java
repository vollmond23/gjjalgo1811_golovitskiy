package com.getjavajob.training.algo1811.golovitskiym.lesson07.tree;

import java.util.*;

/**
 * An abstract base class providing some functionality of the Tree interface
 *
 * @param <E> element
 */
public abstract class AbstractTree<E> implements Tree<E> {
    @Override
    public boolean isInternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) > 0;
    }

    @Override
    public boolean isExternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) == 0;
    }

    @Override
    public boolean isRoot(Node<E> n) throws IllegalArgumentException {
        return n == root();
    }

    @Override
    public boolean isEmpty() {
        return root() == null;
    }

    @Override
    public Iterator<E> iterator() {
        return new ElementIterator();
    }

    /**
     *
     * @return an iterable collection of nodes of the tree in preorder
     */
    public Collection<Node<E>> preOrder() {
        return preOrder(root(), new LinkedList<Node<E>>());
    }

    private Collection<Node<E>> preOrder(Node<E> n, LinkedList<Node<E>> list) {
        if (n == null) {
            return null;
        }
        list.add(n);
        for (Node<E> node : children(n)) {
            preOrder(node, list);
        }
        return list;
    }

    /**
     *
     * @return an iterable collection of nodes of the tree in postorder
     */
    public Collection<Node<E>> postOrder() {
        return postOrder(root(), new LinkedList<Node<E>>());
    }

    private Collection<Node<E>> postOrder(Node<E> n, LinkedList<Node<E>> list) {
        if (n == null) {
            return null;
        }
        for (Node<E> node : children(n)) {
            postOrder(node, list);
        }
        list.add(n);
        return list;
    }

    /**
     *
     * @return an iterable collection of nodes of the tree in breadth-first order
     */
    public Collection<Node<E>> breadthFirst() {
        Queue<Node<E>> queue = new LinkedList<>();
        Collection<Node<E>> list = new LinkedList<>();
        queue.add(root());
        while(!queue.isEmpty()){
            Node<E> current = queue.remove();
            list.add(current);
            if (isInternal(current)) {
                for (Node<E> node : children(current)) {
                    if (node != null) {
                        queue.add(node);
                    }
                }
            }
        }
        return list;
    }

    /**
     * Adapts the iteration produced by {@link Tree#nodes()}
     */
    private class ElementIterator implements Iterator<E> {
        private Iterator<Node<E>> it = nodes().iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public E next() {
            return it.next().getElement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
