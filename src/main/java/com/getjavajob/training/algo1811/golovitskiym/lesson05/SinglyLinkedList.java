package com.getjavajob.training.algo1811.golovitskiym.lesson05;

import java.util.ArrayList;
import java.util.List;

public class SinglyLinkedList<E> {
    private int size;
    private Node<E> head;

    public SinglyLinkedList() {
        size = 0;
        head = null;
    }

    private void checkIndex(int index) {
        if ((index < 0 || index >= size) && size != 0) {
            throw new IndexOutOfBoundsException();
        }
    }

    void add(E e) {
        head = new Node<>(head, e);
        size++;
    }

    void add(int index, E e) {
        checkIndex(index);
        Node<E> node = head;
        if (index == 0) {
            add(e);
        } else {
            for (int i = 0; i < index; i++) {
                node = node.next;
            }
            node.next = (index == size - 1) ? new Node<>(null, e) : new Node<>(node.next, e);
        }
        size++;
    }

    E get(int index) {
        checkIndex(index);
        Node<E> node = head;
        for (int i = 0; i <= index; i++) {
            node = node.next;
        }
        return node.val;
    }

    E remove(int index) {
        checkIndex(index);
        Node<E> node = head;
        E returningValue;
        if (index == 0) {
            returningValue = head.val;
            head = head.next;
        } else {
            for (int i = 0; i < index; i++) {
                node = node.next;
            }
            returningValue = node.next.val;
            node.next = (index == size - 1) ? null : node.next.next;
        }
        size--;
        return returningValue;
    }

    int size() {
        return size;
    }

    List<E> asList() {
        List<E> returningList = new ArrayList<>();
        Node<E> node = head;
        for (int i = 0; i < size; i++) {
            returningList.add(node.val);
            node = node.next;
        }
         return returningList;
    }

    private static class Node<E> {
        Node<E> next;
        E val;

        Node(Node<E> next, E val) {
            this.next = next;
            this.val = val;
        }
    }
}


