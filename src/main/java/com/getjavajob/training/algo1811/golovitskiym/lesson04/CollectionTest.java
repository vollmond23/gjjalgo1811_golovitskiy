package com.getjavajob.training.algo1811.golovitskiym.lesson04;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class CollectionTest {
    public static void main(String[] args) {
        testAdd();
        testAddAll();
        testClear();
        testContains();
        testContainsAll();
        testHashCode();
        testIsEmpty();
        testIterator();
        testRemove();
        testRemoveAll();
        testRetainAll();
        testSize();
        testToArray();
        testToArraySpecified();
    }

    private static void testAdd() {
        ArrayList<Integer> al = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            al.add(i);
        }
        assertEquals("Lesson04.CollectionTest.testAdd", new Integer[] {0, 1, 2, 3, 4}, al.toArray());
    }

    private static void testAddAll() {
        ArrayList<Integer> al = new ArrayList<>();
        ArrayList<Integer> specifiedList = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        al.addAll(specifiedList);
        assertEquals("Lesson04.CollectionTest.testAddAll", new Integer[] {0, 1, 2, 3, 4}, al.toArray());
    }

    private static void testClear() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        al.clear();
        assertEquals("Lesson04.CollectionTest.testClear", new Integer[0], al.toArray());
    }

    private static void testContains() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson04.CollectionTest.testContains", true, al.contains(3));
    }

    private static void testContainsAll() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        ArrayList<Integer> specifiedList = new ArrayList<>(Arrays.asList(0, 1, 2));
        assertEquals("Lesson04.CollectionTest.testContainsAll", true, al.containsAll(specifiedList));
    }

    private static void testHashCode() {
        ArrayList<Integer> al = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            al.add(i);
        }
        int hashCode = 1;
        int length = al.size();
        Integer[] intArray = new Integer[length];
        intArray = al.toArray(intArray);
        for (int i = 0; i < length; i++) {
            Object e = intArray[i];
            hashCode = 31 * hashCode + (e == null ? 0 : e.hashCode());
        }
        assertEquals("Lesson04.CollectionTest.testHashCode", hashCode, al.hashCode());
    }

    private static void testIsEmpty() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson04.CollectionTest.testIsEmpty", false, al.isEmpty());
    }

    private static void testIterator() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        boolean iterable = false;
        for (Class c : al.iterator().getClass().getInterfaces()) {
            if (c.equals(Iterator.class)) {
                iterable = true;
            }
        }
        assertEquals("Lesson04.CollectionTest.testIterator", true, iterable);
    }

    private static void testRemove() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        al.remove(Integer.valueOf(3));
        assertEquals("Lesson04.CollectionTest.testRemove", new Integer[] {0, 1, 2, 4}, al.toArray());
    }

    private static void testRemoveAll() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        ArrayList<Integer> specifiedList = new ArrayList<>(Arrays.asList(0, 1, 2));
        al.removeAll(specifiedList);
        assertEquals("Lesson04.CollectionTest.testRemoveAll", new Integer[] {3, 4}, al.toArray());
    }

    private static void testRetainAll() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        ArrayList<Integer> specifiedList = new ArrayList<>(Arrays.asList(0, 1, 2));
        al.retainAll(specifiedList);
        assertEquals("Lesson04.CollectionTest.testRetainAll", new Integer[] {0, 1, 2}, al.toArray());
    }

    private static void testSize() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson04.CollectionTest.testSize", 5, al.size());
    }

    private static void testToArray() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson04.CollectionTest.testToArray", true, al.toArray().getClass().isArray());
    }

    private static void testToArraySpecified() {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4));
        Integer[] integerArray = new Integer[al.size()];
        assertEquals("Lesson04.CollectionTest.testToArraySpecified", new Integer[] {0, 1, 2, 3, 4}, al.toArray(integerArray));
    }
}
