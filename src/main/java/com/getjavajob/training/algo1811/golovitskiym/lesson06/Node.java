package com.getjavajob.training.algo1811.golovitskiym.lesson06;

class Node<K, V> {
    private K key;
    private V value;
    private Node<K, V> next;
    private long hash;

    public Node(K key, V value, Node<K, V> next) {
        this.key = key;
        this.value = value;
        this.next = next;
        hash = key.hashCode();
    }

    K getKey() {
        return key;
    }

    void setValue(V value) {
        this.value = value;
    }

    void setNext(Node<K, V> next) {
        this.next = next;
    }

    V getValue() {
        return value;
    }

    Node<K, V> getNext() {
        return next;
    }

    boolean hasNext() {
        return this.next != null;
    }

    long getHash() {
        return hash;
    }

    void setHash(int hash) {
        this.hash = hash;
    }
}
