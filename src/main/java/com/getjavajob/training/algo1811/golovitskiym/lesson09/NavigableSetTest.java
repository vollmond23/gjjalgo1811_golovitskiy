package com.getjavajob.training.algo1811.golovitskiym.lesson09;

import java.util.*;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class NavigableSetTest {
    public static void main(String[] args) {
        testLower();
        testFloor();
        testCeiling();
        testHigher();
        testPollFirst();
        testPollLast();
        testDescendingSet();
        testDescendingIterator();
        testSubSet();
        testHeadSet();
        testTailSet();
    }

    private static void testLower() {
        NavigableSet<Integer> ns = fillUpTreeSet(1, 2, 3, 4, 5);
        assertEquals("Lesson09.testLower", 3, ns.lower(4));
    }

    private static void testFloor() {
        NavigableSet<Integer> ns = fillUpTreeSet(1, 2, 3, 4, 5);
        assertEquals("Lesson09.testLower", 4, ns.floor(4));
    }

    private static void testCeiling() {
        NavigableSet<Integer> ns = fillUpTreeSet(1, 2, 3, 4, 5);
        assertEquals("Lesson09.testCeiling", 3, ns.ceiling(3));
    }

    private static void testHigher() {
        NavigableSet<Integer> ns = fillUpTreeSet(1, 2, 3, 4, 5);
        assertEquals("Lesson09.testHigher", 4, ns.higher(3));
    }

    private static void testPollFirst() {
        NavigableSet<Integer> ns = fillUpTreeSet(1, 2, 3, 4, 5);
        int pollingValue = ns.pollFirst();
        Set<Integer> expectedSet = fillUpTreeSet(2, 3, 4, 5);
        assertEquals("Lesson09.testPollFirst", true, pollingValue == 1 && ns.equals(expectedSet));
    }

    private static void testPollLast() {
        NavigableSet<Integer> ns = fillUpTreeSet(1, 2, 3, 4, 5);
        int pollingValue = ns.pollLast();
        Set<Integer> expectedSet = fillUpTreeSet(1, 2, 3, 4);
        assertEquals("Lesson09.testPollLast", true, pollingValue == 5 && ns.equals(expectedSet));
    }

    private static void testDescendingSet() {
        NavigableSet<Integer> ns = fillUpTreeSet(1, 2, 3, 4, 5);
        Integer[] expectedArray = new Integer[] {5, 4, 3, 2, 1};
        assertEquals("Lesson09.testDescendingSet", expectedArray, ns.descendingSet().toArray());
    }

    private static void testDescendingIterator() {
        NavigableSet<Integer> ns = fillUpTreeSet(1, 2, 3, 4, 5);
        List<Integer> descendingList = new ArrayList<>();
        Iterator<Integer> descendingTreeSetIterator = ns.descendingIterator();
        while (descendingTreeSetIterator.hasNext()) {
            descendingList.add(descendingTreeSetIterator.next());
        }
        assertEquals("Lesson09.testDescendingIterator", Arrays.asList(5, 4, 3, 2, 1), descendingList);
    }

    private static void testSubSet() {
        NavigableSet<Integer> ns = fillUpTreeSet(1, 2, 3, 4, 5);
        Integer[] expectedArray = new Integer[] {2, 3, 4};
        assertEquals("Lesson09.testSubSet", expectedArray, ns.subSet(2, 5).toArray());
    }

    private static void testHeadSet() {
        NavigableSet<Integer> ns = fillUpTreeSet(1, 2, 3, 4, 5);
        Integer[] expectedArray = new Integer[] {1, 2, 3, 4};
        assertEquals("Lesson09.testHeadSet", expectedArray, ns.headSet(5).toArray());
    }

    private static void testTailSet() {
        NavigableSet<Integer> ns = fillUpTreeSet(1, 2, 3, 4, 5);
        Integer[] expectedArray = new Integer[] {2, 3, 4, 5};
        assertEquals("Lesson09.testTailSet", expectedArray, ns.tailSet(2).toArray());
    }

    @SafeVarargs
    private static <E> TreeSet<E> fillUpTreeSet(E... args) {
        TreeSet<E> treeSet = new TreeSet<>();
        Collections.addAll(treeSet, args);
        return treeSet;
    }
}
