package com.getjavajob.training.algo1811.golovitskiym.lesson05;

import java.util.*;

class CollectionUtils {
    static <T> boolean filter(Iterable<T> collection, Predicate<T> predicate) {
        boolean result = false;
        if (collection != null && predicate != null) {
            for (Iterator<T> it = collection.iterator(); it.hasNext();) {
                if (!predicate.test(it.next())) {
                    it.remove();
                    result = true;
                }
            }
        }
        return result;
    }

    static <I, O> void transform(Collection inputCollection, Transformer<? super I, ? extends O> transformer) {
        if (inputCollection != null && transformer != null) {
            Collection<O> resultCollection = collect(inputCollection, transformer);
            inputCollection.clear();
            inputCollection.addAll(resultCollection);
        }
    }

    private static <I, O> Collection<O> collect(final Collection<I> inputCollection,
                                               final Transformer<? super I, ? extends O> transformer) {
        return transform(inputCollection, transformer, new ArrayList<O>());
    }

    static <I, O, R extends Collection<O>> R transform(Collection<I> inputCollection, Transformer<? super I, ? extends O> transformer, R outputCollection) {
        if (inputCollection != null && transformer != null) {
            for (I item : inputCollection) {
                O value = transformer.transform(item);
                outputCollection.add(value);
            }
        }
        return outputCollection;
    }

    static <T, C extends Closure<? super T>> C forAllDo(final Iterable<T> collection, final C closure) {
        if (closure != null) {
            for (T element : collection) {
                closure.execute(element);
            }
        } else {
            throw new NullPointerException("Closure must not be null");
        }
        return closure;
    }

    static <T> Collection<T> unmodifiableCollection(Collection<? extends T> c) {
        return new UnmodifiableCollection<>(c);
    }

    interface Predicate<T> {
        boolean test(T object);
    }

    interface Transformer<I, O> {
        O transform(I input);
    }

    interface Closure<T> {
        void execute(T input);

    }
}
