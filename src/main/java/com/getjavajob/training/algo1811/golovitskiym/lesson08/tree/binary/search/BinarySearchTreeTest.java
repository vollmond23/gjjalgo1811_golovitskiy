package com.getjavajob.training.algo1811.golovitskiym.lesson08.tree.binary.search;

import com.getjavajob.training.algo1811.golovitskiym.lesson07.tree.Node;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class BinarySearchTreeTest {
    public static void main(String[] args) {
        testAdd();
        testTreeSearch();
        testRemoveCase1();
        testRemoveCase2();
        testRemoveCase3();
    }

    private static void testAdd() {
        BinarySearchTree<Integer> bst = fillUpBST(Arrays.asList(4, 2, 3, 1, 5, 7, 6), compInteger);
        String expected = "(4(2(1,3),5(null,7(6,null))))";
        String actual = bst.toString();
        assertEquals("Lesson08.testAdd", expected, actual);
    }

    private static void testTreeSearch() {
        BinarySearchTree<Integer> bst = fillUpBST(Arrays.asList(4, 2, 3, 1, 5, 7, 6), compInteger);
        Node<Integer> searchNode = bst.treeSearch(bst.root(), 3);
        assertEquals("Lesson08.testTreeSearch", true, searchNode != null && searchNode.getElement() == 3);
    }

    private static void testRemoveCase1() {
        BinarySearchTree<Integer> bst = fillUpBST(Arrays.asList(4, 2, 3, 1, 5, 7, 6), compInteger);
        String expected = "(4(2(null,3),5(null,7(6,null))))";
        bst.remove(1);
        String actual = bst.toString();
        assertEquals("Lesson08.testRemoveCase1 (no children) ", expected, actual);
    }

    private static void testRemoveCase2() {
        BinarySearchTree<Integer> bst = fillUpBST(Arrays.asList(4, 2, 3, 1, 5, 7, 6), compInteger);
        String expected = "(4(2(1,3),5(null,6)))";
        bst.remove(7);
        String actual = bst.toString();
        assertEquals("Lesson08.testRemoveCase2 (one child) ", expected, actual);
    }

    private static void testRemoveCase3() {
        BinarySearchTree<Integer> bst = fillUpBST(Arrays.asList(4, 2, 3, 1, 5, 7, 6), compInteger);
        String expected = "(4(3(1,null),5(null,7(6,null))))";
        bst.remove(2);
        String actual = bst.toString();
        assertEquals("Lesson08.testRemoveCase3 (two children) ", expected, actual);
    }

    private static <E> BinarySearchTree<E> fillUpBST(List<E> list, Comparator<E> comp) {
        BinarySearchTree<E> bst = new BinarySearchTree<>(comp);
        for (E element : list) {
            bst.add(element);
        }
        return bst;
    }

    private static Comparator<Integer> compInteger = new Comparator<Integer>() {
        @Override
        public int compare(Integer o1, Integer o2) {
            return o1.compareTo(o2);
        }
    };
}
