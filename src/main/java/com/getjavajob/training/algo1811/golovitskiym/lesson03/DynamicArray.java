package com.getjavajob.training.algo1811.golovitskiym.lesson03;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

public class DynamicArray {
    private Object[] workingArray;
    private int size;
    private transient int modCount = 0;

    public DynamicArray() {
        workingArray = new Object[10];
        size = 0;
    }

    public DynamicArray(int size) {
        workingArray = new Object[size];
        this.size = 0;
    }

    private void expandWorkingArray() {
        workingArray = Arrays.copyOf(workingArray, (int) (workingArray.length * 1.5));
    }

    private void checkWorkingArraySize() {
        if (size >= workingArray.length) {
            expandWorkingArray();
        }
    }

    boolean add(Object e) {
        checkWorkingArraySize();
        workingArray[size] = e;
        size++;
        modCount++;
        return true;
    }

    void add(int i, Object e) {
        checkWorkingArraySize();
        checkRange(i);
        System.arraycopy(workingArray, i, workingArray, i + 1, size - i);
        workingArray[i] = e;
        size++;
        modCount++;
    }

    Object set(int i, Object e) {
        checkRange(i);
        Object returningValue = workingArray[i];
        workingArray[i] = e;
        return returningValue;
    }

    Object get(int i) {
        checkRange(i);
        return workingArray[i];
    }

    Object remove(int i) {
        checkRange(i);
        Object returningValue = workingArray[i];
        System.arraycopy(workingArray, i + 1, workingArray, i, size - i - 1);
        workingArray[size - 1] = null;
        size--;
        modCount++;
        return returningValue;
    }

    boolean remove(Object e) {
        boolean objectWasRemoved = false;
        int index = indexOf(e);
        if (index > -1) {
            remove(index);
            objectWasRemoved = true;
        }
        return objectWasRemoved;
    }

    int size() {
        return size;
    }

    int indexOf(Object e) {
        int returningValue = -1;
        for (int i = 0; i < size; i++) {
            if (workingArray[i].equals(e)) {
                returningValue = i;
            }
        }
        return returningValue;
    }

    boolean contains(Object e) {
        return indexOf(e) > -1;
    }

    Object[] toArray() {
        return Arrays.copyOf(workingArray, size);
    }

    void checkRange(int index) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException("ArrayIndexOutOfBoundsException");
        }
    }

    DynamicArray.ListIterator listIterator() {
        return new ListIterator();
    }

    public class ListIterator implements java.util.ListIterator<Object> {
        private int currentIndex = 0;
        private int lastIndex = -1;
        private int expectedModCount = modCount;

        final void checkForComodification() {
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
        }

        @Override
        public boolean hasNext() {
            return currentIndex < size;
        }

        @Override
        public Object next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            lastIndex = currentIndex++;
            return workingArray[lastIndex];
        }

        @Override
        public boolean hasPrevious() {
            return currentIndex > 0;
        }

        @Override
        public Object previous() {
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            lastIndex = --currentIndex;
            return workingArray[lastIndex];
        }

        @Override
        public int nextIndex() {
            return currentIndex;
        }

        @Override
        public int previousIndex() {
            return currentIndex - 1;
        }

        @Override
        public void remove() {
            if (lastIndex < 0) {
                throw new IllegalStateException();
            }
            checkForComodification();
            try {
                DynamicArray.this.remove(lastIndex);
                currentIndex = lastIndex;
                lastIndex = -1;
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public void set(Object o) {
            if (lastIndex < 0)
                throw new IllegalStateException();
            checkForComodification();
            try {
                DynamicArray.this.set(lastIndex, o);
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public void add(Object o) {
            checkForComodification();
            try {
                DynamicArray.this.add(currentIndex, o);
                currentIndex++;
                lastIndex = -1;
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
