package com.getjavajob.training.algo1811.golovitskiym.lesson07.tree.binary;

import com.getjavajob.training.algo1811.golovitskiym.lesson07.tree.AbstractTree;
import com.getjavajob.training.algo1811.golovitskiym.lesson07.tree.Node;

import java.util.Collection;
import java.util.LinkedList;

public abstract class AbstractBinaryTree<E> extends AbstractTree<E> implements BinaryTree<E> {
    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        Node<E> parent = parent(n);
        if (left(parent) == n) {
            return right(parent);
        }
        if (right(parent) == n) {
            return left(parent);
        }
        return null;
    }

    @Override
    public Collection<Node<E>> children(Node<E> n) throws IllegalArgumentException {
        Collection<Node<E>> children = new LinkedList<>();
        children.add(left(n));
        children.add(right(n));
        return children;
    }

    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        int childrenNumber = 0;
        if (left(n) != null) {
            childrenNumber++;
        }
        if (right(n) != null) {
            childrenNumber++;
        }
        return childrenNumber;
    }

    /**
     *
     * @return an iterable collection of nodes of the tree in inorder
     */
    Collection<Node<E>> inOrder() {
        return inOrder(root(), new LinkedList<Node<E>>());
    }

    private Collection<Node<E>> inOrder(Node<E> n, LinkedList<Node<E>> list) {
        if (n == null) {
            return null;
        }
        inOrder(left(n), list);
        list.add(n);
        inOrder(right(n), list);
        return list;
    }
}