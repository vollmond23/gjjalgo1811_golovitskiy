package com.getjavajob.training.algo1811.golovitskiym.lesson10;

import java.math.BigInteger;
import java.util.*;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class SortingTest {
    public static void main(String[] args) {
        testBubbleSort();
        testInsertSort();
        testQuickSort();
        testMergeSort();
        howManyGbINeed();
    }

    private static void testBubbleSort() {
        Sorting<Integer> bsort = new Sorting<>(new Integer[] {0, 1, -2, 3, -4, 5, -6, 7, -8, 9});
        String expectedString = "[-8, -6, -4, -2, 0, 1, 3, 5, 7, 9]";
        bsort.bubbleSort();
        assertEquals("Lesson10.testBubbleSort (Arrays)", expectedString, bsort.toString());
        Collection<Integer> collection = new LinkedList<>(Arrays.asList(0, 1, -2, 3, -4, 5, -6, 7, -8, 9));
        bsort = new Sorting<>(collection);
        bsort.bubbleSort();
        assertEquals("Lesson10.testBubbleSort (Collection)", expectedString, bsort.toString());
    }

    private static void testInsertSort() {
        Sorting<Integer> bsort = new Sorting<>(new Integer[] {0, 1, -2, 3, -4, 5, -6, 7, -8, 9});
        String expectedString = "[-8, -6, -4, -2, 0, 1, 3, 5, 7, 9]";
        bsort.insertSort();
        assertEquals("Lesson10.testInsertSort (Arrays)", expectedString, bsort.toString());
        Collection<Integer> collection = new LinkedList<>(Arrays.asList(0, 1, -2, 3, -4, 5, -6, 7, -8, 9));
        bsort = new Sorting<>(collection);
        bsort.insertSort();
        assertEquals("Lesson10.testInsertSort (Collection)", expectedString, bsort.toString());
    }

    private static void testQuickSort() {
        Sorting<Integer> bsort = new Sorting<>(new Integer[] {0, 1, -2, 3, -4, 5, -6, 7, -8, 9});
        String expectedString = "[-8, -6, -4, -2, 0, 1, 3, 5, 7, 9]";
        bsort.quickSort();
        assertEquals("Lesson10.testQuickSort (Arrays)", expectedString, bsort.toString());
        Collection<Integer> collection = new LinkedList<>(Arrays.asList(0, 1, -2, 3, -4, 5, -6, 7, -8, 9));
        bsort = new Sorting<>(collection);
        bsort.quickSort();
        assertEquals("Lesson10.testQuickSort (Collection)", expectedString, bsort.toString());
    }

    private static void testMergeSort() {
        Sorting<Integer> bsort = new Sorting<>(new Integer[] {0, 1, -2, 3, -4, 5, -6, 7, -8, 9});
        String expectedString = "[-8, -6, -4, -2, 0, 1, 3, 5, 7, 9]";
        bsort.mergeSort();
        assertEquals("Lesson10.testMergeSort (Arrays)", expectedString, bsort.toString());
        Collection<Integer> collection = new LinkedList<>(Arrays.asList(0, 1, -2, 3, -4, 5, -6, 7, -8, 9));
        bsort = new Sorting<>(collection);
        bsort.mergeSort();
        assertEquals("Lesson10.testMergeSort (Collection)", expectedString, bsort.toString());
    }

    /**
     * Shows how many Gb of RAM I need to make tests for data size Integer.MAX_VALUE for Quick sort and Merge sort
     */
    private static void howManyGbINeed() {
        final int MAX_VALUE = Integer.MAX_VALUE;
        final int BYTES_IN_INTEGER = 16;
        final int BYTES_IN_GB = 1073741824;
        BigInteger bigInteger = new BigInteger(String.valueOf(MAX_VALUE));
        // Number of bytes that I need to reserve for array of Integer
        bigInteger = bigInteger.multiply(BigInteger.valueOf(BYTES_IN_INTEGER));
        // Number of bytes that I need to reserve for array of Integer
        bigInteger = bigInteger.divide(BigInteger.valueOf(BYTES_IN_GB));
        // Quicksort is in-place algorithm
        System.out.println("I need " + bigInteger + " Gb RAM for Quicksort test with BIG array");
        // Mergesort requires additional array same size
        System.out.println("I need " + bigInteger.multiply(BigInteger.valueOf(2)) + " Gb RAM for Mergesort test with BIG array");
    }
}
