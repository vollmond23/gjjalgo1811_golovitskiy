package com.getjavajob.training.algo1811.golovitskiym.lesson04;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class DoublyLinkedListTest {
    public static void main(String[] args) {
        testAddBeginning();
        testAddMiddle();
        testAddEnd();
        testRemoveBeginning();
        testRemoveMiddle();
        testRemoveEnd();
    }

    private static void testAddBeginning() {
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();
        for (int i = 0; i < 5; i++) {
            dll.add(0, i);
        }
        assertEquals("Lesson04.testAddBeginning", new Object[] {4, 3, 2, 1, 0}, dll.toArray());
    }

    private static void testAddMiddle() {
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();
        for (int i = 0; i < 10; i++) {
            dll.add(1);
        }
        for (int i = 0; i < 5; i++) {
            dll.add(5, i);
        }
        assertEquals("Lesson04.testAddMiddle", new Object[] {1, 1, 1, 1, 1, 4, 3, 2, 1, 0, 1, 1, 1, 1, 1}, dll.toArray());
    }

    private static void testAddEnd() {
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();
        for (int i = 0; i < 10; i++) {
            dll.add(1);
        }
        for (int i = 0; i < 5; i++) {
            dll.add(i);
        }
        assertEquals("Lesson04.testAddEnd", new Object[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 2, 3, 4}, dll.toArray());
    }

    private static void testRemoveBeginning() {
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();
        for (int i = 0; i < 10; i++) {
            dll.add(i);
        }
        for (int i = 0; i < 5; i++) {
            dll.remove(0);
        }
        assertEquals("Lesson04.testRemoveBeginning", new Object[] {5, 6, 7, 8, 9}, dll.toArray());
    }

    private static void testRemoveMiddle() {
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();
        for (int i = 0; i < 10; i++) {
            dll.add(i);
        }
        for (int i = 0; i < 5; i++) {
            dll.remove(5);
        }
        assertEquals("Lesson04.testRemoveMiddle", new Object[] {0, 1, 2, 3, 4}, dll.toArray());
    }

    private static void testRemoveEnd() {
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();
        for (int i = 0; i < 10; i++) {
            dll.add(i);
        }
        for (int i = 0; i < 5; i++) {
            dll.remove(dll.size() - 1);
        }
        assertEquals("Lesson04.testRemoveEnd", new Object[] {0, 1, 2, 3, 4}, dll.toArray());
    }
}
