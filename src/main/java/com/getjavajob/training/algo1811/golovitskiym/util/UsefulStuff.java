package com.getjavajob.training.algo1811.golovitskiym.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class UsefulStuff {
    public static void clearFile(String fileName) throws IOException {
        FileWriter fw = new FileWriter(new File(fileName));
        fw.close();
    }
}
