package com.getjavajob.training.algo1811.golovitskiym.lesson10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

class Sorting<E extends Comparable<? super E>> {
    private List<E> objects;
    private int size;

    Sorting(E[] objectsArray) {
        this.objects = new ArrayList<>(Arrays.asList(objectsArray));
        size = objects.size();
        checkSortingList();
    }

    Sorting(Collection<E> objectsCollection) {
        this.objects = new ArrayList<>(objectsCollection);
        size = objects.size();
        checkSortingList();
    }

    private void checkSortingList() {
        if (objects == null || size == 0)
            throw new IllegalArgumentException("Nothing to sort");
    }

    void bubbleSort() {
        E tempObject;
        do {
            int newSize = 0;
            for (int i = 1; i < size; i++) {
                if (objects.get(i - 1).compareTo(objects.get(i)) > 0) {
                    tempObject = objects.get(i - 1);
                    objects.set(i - 1, objects.get(i));
                    objects.set(i, tempObject);
                    newSize = i;
                }
            }
            size = newSize;
        } while (size > 1);
    }

    void insertSort() {
        E tempObject;
        for (int i = 1; i < size; i++) {
            for (int j = i; j > 0; j--) {
                if (objects.get(j - 1).compareTo(objects.get(j)) > 0) {
                    tempObject = objects.get(j - 1);
                    objects.set(j - 1, objects.get(j));
                    objects.set(j, tempObject);
                }
            }
        }
    }

    void quickSort() {
        quickSortAlgo(objects, 0, size - 1);
    }

    private void quickSortAlgo(List<E> array, int lo, int hi) {
        if (lo < hi) {
            int p = partition(array, lo, hi);
            quickSortAlgo(array, lo, p - 1);
            quickSortAlgo(array, p + 1, hi);
        }
    }

    private int partition(List<E> array, int lo, int hi) {
        E pivot = array.get(hi);
        E tempObject;
        int i = lo;
        for (int j = lo; j <= hi; j++) {
            if (array.get(j).compareTo(pivot) < 0) {
                tempObject = array.get(i);
                array.set(i, array.get(j));
                array.set(j, tempObject);
                i++;
            }
        }
        tempObject = array.get(i);
        array.set(i, array.get(hi));
        array.set(hi, tempObject);
        return i;
    }

    void mergeSort() {
        objects = mergeSortAlgo(objects);
    }

    private List<E> mergeSortAlgo(List<E> m) {
        int size = m.size();
        if (size <= 1) {
            return m;
        }
        List<E> leftList = new ArrayList<>();
        List<E> rightList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            if (i < size / 2) {
                leftList.add(m.get(i));
            } else {
                rightList.add(m.get(i));
            }
        }
        leftList = mergeSortAlgo(leftList);
        rightList = mergeSortAlgo(rightList);
        return merge(leftList, rightList);
    }

    private List<E> merge(List<E> leftList, List<E> rightList) {
        List<E> result = new ArrayList<>();
        while (!leftList.isEmpty() && !rightList.isEmpty()) {
            if (leftList.get(0).compareTo(rightList.get(0)) <= 0) {
                result.add(leftList.get(0));
                leftList.remove(0);
            } else {
                result.add(rightList.get(0));
                rightList.remove(0);
            }
        }
        while (!leftList.isEmpty()) {
            result.add(leftList.get(0));
            leftList.remove(0);
        }
        while (!rightList.isEmpty()) {
            result.add(rightList.get(0));
            rightList.remove(0);
        }
        return result;
    }

    @Override
    public String toString() {
        return objects.toString();
    }
}
