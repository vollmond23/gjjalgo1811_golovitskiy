package com.getjavajob.training.algo1811.golovitskiym.lesson08.tree.binary.search;

import com.getjavajob.training.algo1811.golovitskiym.lesson07.tree.Node;
import com.getjavajob.training.algo1811.golovitskiym.lesson07.tree.binary.LinkedBinaryTree;

import java.util.Comparator;
import java.util.Objects;

/**
 * @author Vital Severyn
 * @since 31.07.15
 */
public class BinarySearchTree<E> extends LinkedBinaryTree<E> {
    private Comparator<E> comparator;

    public BinarySearchTree() {
        comparator = new Comparator<E>() {
            @Override
            public int compare(E o1, E o2) {
                return o1.hashCode() - o2.hashCode();
            }
        };
    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    /**
     * Method for comparing two values
     *
     * @param val1 - not null value
     * @param val2 - not null value
     * @return - int value of comparing val1 and val2
     */
    private int compare(E val1, E val2) {
        Objects.requireNonNull(val1);
        Objects.requireNonNull(val2);
        Objects.requireNonNull(comparator, "Comparator must be not null.");
        return comparator.compare(val1, val2);
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        E nodeElement = n.getElement();
        NodeImpl<E> nImpl = validate(n);
        if (compare(e, nodeElement) < 0) {
            if (nImpl.getLeftChild() == null) {
                return addLeft(n, e);
            } else {
                return add(nImpl.getLeftChild(), e);
            }
        } else if (compare(e, nodeElement) > 0) {
            if (nImpl.getRightChild() == null) {
                return addRight(n, e);
            } else {
                return add(nImpl.getRightChild(), e);
            }
        } else {
            throw new IllegalArgumentException("There is already such element in the tree.");
        }
    }

    public Node<E> add(E e) throws IllegalArgumentException {
        if (root() == null) {
            return addRoot(e);
        } else {
            return add(root(), e);
        }
    }

    private void remove(Node<E> n, E e) throws IllegalArgumentException {
        if (n == null) {
            return;
        }
        NodeImpl<E> nImpl = validate(n);
        Node<E> leftChild = nImpl.getLeftChild();
        Node<E> rightChild = nImpl.getRightChild();
        int compare = compare(e, n.getElement());
        if (compare < 0) {
            remove(leftChild, e);
        } else if (compare > 0) {
            remove(rightChild, e);
        } else {
            if (childrenNumber(n) == 2) {
                Node<E> successor = findMin(rightChild);
                nImpl.setElement(successor.getElement());
                remove(successor, successor.getElement());
            } else if (leftChild != null) {
                replaceNodeInParent(n, leftChild);
            } else if (rightChild != null) {
                replaceNodeInParent(n, rightChild);
            } else {
                replaceNodeInParent(n, null);
            }
        }
    }

    public void remove(E e) throws IllegalArgumentException {
        remove(root(), e);
    }

    protected Node<E> findMin(Node<E> n) {
        NodeImpl<E> currentNode = validate(n);
        while (currentNode.getLeftChild() != null) {
            currentNode = validate(currentNode.getLeftChild());
        }
        return currentNode;
    }

    private void replaceNodeInParent(Node<E> n, Node<E> child) {
        NodeImpl<E> parent = validate(parent(n));
        if (parent != null) {
            if (n == parent.getLeftChild()) {
                parent.setLeftChild(child);
            } else {
                parent.setRightChild(child);
            }
        }
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n - root of subtree
     * @param val - value of searching node
     * @return - node with value == val
     */
    public Node<E> treeSearch(Node<E> n, E val) {
        if (n == null) {
            return null;
        }
        NodeImpl<E> nodeImpl = validate(n);
        int compare = compare(val, n.getElement());
        if (compare == 0) {
            return n;
        } else if (compare < 0) {
            return treeSearch(nodeImpl.getLeftChild(), val);
        } else {
            return treeSearch(nodeImpl.getRightChild(), val);
        }
    }

    protected void afterElementRemoved(Node<E> n) {

    }

    protected void afterElementAdded(Node<E> n) {

    }

    @Override
    public String toString() {
        return treeString(root(), new StringBuilder()).toString();
    }

    private StringBuilder treeString(Node<E> node, StringBuilder str) {
        if (node == null) {
            return null;
        }
        Node<E> parentNode = parent(node);
        if (right(parentNode) == node && left(parentNode) != null) {
            str.append(",");
        } else if (right(parentNode) == node && left(parentNode) == null) {
            str.append("(null,");
        } else {
            str.append("(");
        }
        str.append(node.getElement());
        for (Node<E> n : children(node)) {
            treeString(n, str);
        }
        if (right(parentNode) == null && parentNode != null) {
            str.append(",null)");
        } else if (node == right(parentNode) || parentNode == null) {
            str.append(")");
        }
        return str;
    }
}
