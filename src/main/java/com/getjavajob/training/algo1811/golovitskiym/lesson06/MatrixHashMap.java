package com.getjavajob.training.algo1811.golovitskiym.lesson06;

import java.util.HashMap;
import java.util.Map;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class MatrixHashMap<V> implements Matrix<V> {
    private Map<MatrixKey, V> map = new HashMap<>();

    public static void main(String[] args) {
        testMatrix();
    }

    private static void testMatrix() {
        MatrixHashMap<String> matrixHashMap = new MatrixHashMap<>();
        for (int i = 0; i < 1_000_000; i++) {
            matrixHashMap.set(i, 1_000_000, "String " + i);
        }
        assertEquals("Lesson06.testMatrix", "String 555555", matrixHashMap.get(555_555, 1_000_000));
    }

    @Override
    public V get(int i, int j) {
        MatrixKey mk = new MatrixKey(i, j);
        return map.get(mk);
    }

    @Override
    public void set(int i, int j, V value) {
        map.put(new MatrixKey(i, j), value);
    }

    private class MatrixKey {
        int i;
        int j;

        MatrixKey(int i, int j) {
            if (i < 0 || j < 0) {
                throw new IndexOutOfBoundsException();
            }
            this.i = i;
            this.j = j;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            MatrixKey matrixKey = (MatrixKey) o;

            if (i != matrixKey.i) return false;
            return j == matrixKey.j;
        }

        @Override
        public int hashCode() {
            int result = 17;
            result = 31 * result + (Integer.valueOf(i)).hashCode();
            result = 31 * result + (Integer.valueOf(j)).hashCode();
            return result;
        }
    }
}
