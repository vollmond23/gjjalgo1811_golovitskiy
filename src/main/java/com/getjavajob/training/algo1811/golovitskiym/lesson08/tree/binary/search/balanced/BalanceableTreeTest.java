package com.getjavajob.training.algo1811.golovitskiym.lesson08.tree.binary.search.balanced;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class BalanceableTreeTest {
    public static void main(String[] args) {
        testRotate();
        testReduceSubtreeHeight();
    }

    private static void testRotate() {
        BalanceableTree<Integer> bt = fillUpBT(Arrays.asList(4, 2, 3, 1, 5, 7, 6), compInteger);
        bt.rotate(bt.treeSearch(bt.root(), 3));
        String expected = "(4(3(2(1,null),null),5(null,7(6,null))))";
        String actual = bt.toString();
        assertEquals("Lesson08.testRotate", expected, actual);
    }

    private static void testReduceSubtreeHeight() {
        BalanceableTree<Integer> bt = fillUpBT(Arrays.asList(1, 2, 3, 4, 5, 6, 7), compInteger);
        bt.reduceSubtreeHeight(bt.treeSearch(bt.root(), 5));
        String expected = "(1(null,2(null,4(3,5(null,6(null,7))))))";
        String actual = bt.toString();
        assertEquals("Lesson08.testReduceSubtreeHeight", expected, actual);
    }

    private static <E> BalanceableTree<E> fillUpBT(List<E> list, Comparator<E> comp) {
        BalanceableTree<E> bt = new BalanceableTree<>(comp);
        for (E element : list) {
            bt.add(element);
        }
        return bt;
    }

    private static Comparator<Integer> compInteger = new Comparator<Integer>() {
        @Override
        public int compare(Integer o1, Integer o2) {
            return o1.compareTo(o2);
        }
    };
}
