package com.getjavajob.training.algo1811.golovitskiym.lesson03;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;
import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.fail;

public class DynamicArrayTest {
    public static void main(String[] args) {
        testAddBeginning();
        testAddMiddle();
        testAddEnd();
        testRemoveBeginning();
        testRemoveMiddle();
        testRemoveEnd();
        testAddWithException();
        testRemoveWithException();
        testGetWithException();
        testListIterator();
        testListIteratorAdd();
    }

    private static void testAddBeginning() {
        DynamicArray da = new DynamicArray();
        for (int i = 0; i < 5; i++) {
            da.add(0, i);
        }
        assertEquals("Lesson03.testAddBeginning", new Object[] {4, 3, 2, 1, 0}, da.toArray());
    }

    private static void testAddMiddle() {
        DynamicArray da = new DynamicArray();
        for (int i = 0; i < 10; i++) {
            da.add(1);
        }
        for (int i = 0; i < 5; i++) {
            da.add(5, i);
        }
        assertEquals("Lesson03.testAddMiddle", new Object[] {1, 1, 1, 1, 1, 4, 3, 2, 1, 0, 1, 1, 1, 1, 1}, da.toArray());
    }

    private static void testAddEnd() {
        DynamicArray da = new DynamicArray();
        for (int i = 0; i < 10; i++) {
            da.add(1);
        }
        for (int i = 0; i < 5; i++) {
            da.add(i);
        }
        assertEquals("Lesson03.testAddEnd", new Object[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 2, 3, 4}, da.toArray());
    }

    private static void testRemoveBeginning() {
        DynamicArray da = new DynamicArray();
        for (int i = 0; i < 10; i++) {
            da.add(i);
        }
        for (int i = 0; i < 5; i++) {
            da.remove(0);
        }
        assertEquals("Lesson03.testRemoveBeginning", new Object[] {5, 6, 7, 8, 9}, da.toArray());
    }

    private static void testRemoveMiddle() {
        DynamicArray da = new DynamicArray();
        for (int i = 0; i < 10; i++) {
            da.add(i);
        }
        for (int i = 0; i < 5; i++) {
            da.remove(5);
        }
        assertEquals("Lesson03.testRemoveMiddle", new Object[] {0, 1, 2, 3, 4}, da.toArray());
    }

    private static void testRemoveEnd() {
        DynamicArray da = new DynamicArray();
        for (int i = 0; i < 10; i++) {
            da.add(i);
        }
        for (int i = 0; i < 5; i++) {
            da.remove(da.size() - 1);
        }
        assertEquals("Lesson03.testRemoveEnd", new Object[] {0, 1, 2, 3, 4}, da.toArray());
    }

    private static void testAddWithException() {
        DynamicArray da = new DynamicArray();
        try {
            da.add(11, 11);
            fail("Exception wasn't thrown");
        } catch (Exception e) {
            assertEquals("Lesson03.testAddWithException", "ArrayIndexOutOfBoundsException", e.getMessage());
        }
    }

    private static void testRemoveWithException() {
        DynamicArray da = new DynamicArray();
        try {
            da.remove(2);
            fail("Exception wasn't thrown");
        } catch (Exception e) {
            assertEquals("Lesson03.testRemoveWithException", "ArrayIndexOutOfBoundsException", e.getMessage());
        }
    }

    private static void testGetWithException() {
        DynamicArray da = new DynamicArray();
        try {
            da.get(2);
            fail("Exception wasn't thrown");
        } catch (Exception e) {
            assertEquals("Lesson03.testGetWithException", "ArrayIndexOutOfBoundsException", e.getMessage());
        }
    }

    private static void testListIterator() {
        DynamicArray da = new DynamicArray();
        for (int i = 0; i < 5; i++) {
            da.add(i);
        }
        Object[] actualArray = new Object[5];
        int i = 0;
        DynamicArray.ListIterator it = da.listIterator();
        while (it.hasNext()) {
            actualArray[i++] = it.next();
        }
        assertEquals("Lesson03.testListIterator", new Object[]{0, 1, 2, 3, 4}, actualArray);
    }

    private static void testListIteratorAdd() {
        DynamicArray da = new DynamicArray();
        for (int i = 0; i < 5; i++) {
            da.add(i);
        }
        Object[] actualArray = new Object[6];
        int i = 5;
        DynamicArray.ListIterator it = da.listIterator();
        /*
          Just want to test backward order and how "previous" things work. To do this I need to set cursor in the end.
         */
        while (it.hasNext()) {
            it.next();
        }
        while (it.hasPrevious()) {
            actualArray[i--] = it.previous();
            if (i == 2) {
                it.add(13);
            }
        }
        assertEquals("Lesson03.testListIteratorAdd", new Object[] {0, 1, 13, 2, 3, 4}, actualArray);
    }
}
