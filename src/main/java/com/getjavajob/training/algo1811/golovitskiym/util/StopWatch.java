package com.getjavajob.training.algo1811.golovitskiym.util;

public class StopWatch {
    private static long startTime;

    public long start() {
        startTime = System.currentTimeMillis();
        return startTime;
    }

    public long getElapsedTime() {
        return System.currentTimeMillis() - startTime;
    }
}
