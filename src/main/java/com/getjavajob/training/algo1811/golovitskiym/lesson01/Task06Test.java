package com.getjavajob.training.algo1811.golovitskiym.lesson01;

import static com.getjavajob.training.algo1811.golovitskiym.lesson01.Task06.*;
import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.*;

public class Task06Test {
    public static void main(String[] args) {
        testSubtaskA();
        testSubtaskB();
        testSubtaskC();
        testSubtaskD();
        testSubtaskE();
        testSubtaskF();
        testSubtaskG();
        testSubtaskH();
        testSubtaskI();
    }

    private static void testSubtaskA() {
        assertEquals("Lesson01.Task06.testSubtaskA", 0b0000_1000, subtaskA(3));
    }

    private static void testSubtaskB() {
        assertEquals("Lesson01.Task06.testSubtaskB", 0b0001_1000, subtaskB(3, 4));
    }

    private static void testSubtaskC() {
        assertEquals("Lesson01.Task06.testSubtaskC", 0b0000_1000, subtaskC(0b0000_1111,3));
    }

    private static void testSubtaskD() {
        assertEquals("Lesson01.Task06.testSubtaskD", 0b0000_0100, subtaskD(0b0000_0000,3));
    }

    private static void testSubtaskE() {
        assertEquals("Lesson01.Task06.testSubtaskE", 0b0000_0000, subtaskE(0b000_0100,3));
    }

    private static void testSubtaskF() {
        assertEquals("Lesson01.Task06.testSubtaskF", 0b0000_1011, subtaskF(0b00001111,3));
    }

    private static void testSubtaskG() {
        assertEquals("Lesson01.Task06.testSubtaskG", 0b0000_0011, subtaskG(0b1111_1011,3));
    }

    private static void testSubtaskH() {
        assertEquals("Lesson01.Task06.testSubtaskH", 0, subtaskH(0b0000_1011,3));
    }

    private static void testSubtaskI() {
        assertEquals("Lesson01.Task06.testSubtaskI", "10101010", subtaskI((byte) -86));
    }
}
