package com.getjavajob.training.algo1811.golovitskiym.lesson05;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;

public class UnmodifiableCollection<E> extends AbstractCollection<E>{
    final Collection<? extends E> c;

    UnmodifiableCollection(Collection<? extends E> c) {
        if (c==null)
            throw new NullPointerException();
        this.c = c;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private final Iterator<? extends E> i = c.iterator();
            public boolean hasNext() {
                return i.hasNext();
            }
            public E next() {
                return i.next();
            }
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Override
    public int size() {
        return c.size();
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }
}
