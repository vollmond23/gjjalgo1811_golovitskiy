package com.getjavajob.training.algo1811.golovitskiym.lesson09.tree.binary.search.balanced;

import com.getjavajob.training.algo1811.golovitskiym.lesson07.tree.Node;
import com.getjavajob.training.algo1811.golovitskiym.lesson08.tree.binary.search.balanced.BalanceableTree;

/**
 * @author Vital Severyn
 * @since 05.08.15
 */
public class RedBlackTree<E> extends BalanceableTree<E> {
    @Override
    public Node<E> add(E e) throws IllegalArgumentException {
        Node<E> addedNode = super.add(e);
        afterElementAdded(addedNode);
        return addedNode;
    }

    @Override
    public void remove(E e) throws IllegalArgumentException {
        removeNode(treeSearch(root(), e));
    }

    private boolean isBlack(Node<E> n) {
        if (n == null) {
            return true;
        }
        NodeImpl<E> rbNodeImpl = validate(n);
        return rbNodeImpl.getColor() == Color.BLACK;
    }

    private boolean isRed(Node<E> n) {
        if (n == null) {
            return false;
        }
        NodeImpl<E> rbNodeImpl = validate(n);
        return rbNodeImpl.getColor() == Color.RED;
    }

    private void makeBlack(Node<E> n) {
        if (n != null) {
            NodeImpl<E> rbNodeImpl = validate(n);
            rbNodeImpl.setColor(Color.BLACK);
        }
    }

    private void makeRed(Node<E> n) {
        if (n != null) {
            NodeImpl<E> rbNodeImpl = validate(n);
            rbNodeImpl.setColor(Color.RED);
        } else {
            throw new IllegalArgumentException("Null node is always black.");
        }
    }

    private void insertCase1(Node<E> n) {
        if (parent(n) == null) {
            makeBlack(n);
        } else {
            insertCase2(n);
        }
    }

    private void insertCase2(Node<E> n) {
        Node<E> parent = parent(n);
        if (isRed(parent)) {
            insertCase3(n);
        }
    }

    private void insertCase3(Node<E> n) {
        Node<E> parent = parent(n);
        Node<E> grandPa = parent(parent);
        Node<E> uncle = sibling(parent);
        if (isRed(uncle) && uncle != null) {
            makeBlack(parent);
            makeBlack(uncle);
            makeRed(grandPa);
            insertCase1(grandPa);
        } else {
            insertCase4(n);
        }
    }

    private void insertCase4(Node<E> n) {
        NodeImpl<E> parentImpl = validate(parent(n));
        NodeImpl<E> grandPaImpl = validate(parent(parentImpl));
        if (grandPaImpl.getLeftChild() == parentImpl && parentImpl.getRightChild() == n ||
                grandPaImpl.getRightChild() == parentImpl && parentImpl.getLeftChild() == n) {
            rotate(n);
        }
        insertCase5(n);
    }

    private void insertCase5(Node<E> n) {
        NodeImpl<E> parentImpl = validate(parent(n));
        NodeImpl<E> grandPaImpl = validate(parent(parentImpl));
        makeBlack(parentImpl);
        makeRed(grandPaImpl);
        if (grandPaImpl.getRightChild() == parentImpl && parentImpl.getRightChild() == n ||
                grandPaImpl.getLeftChild() == parentImpl && parentImpl.getLeftChild() == n) {
            rotate(parentImpl);
        }
    }

    private void removeNode(Node<E> n) {
        NodeImpl<E> nodeImpl = validate(n);
        Node<E> leftChild = nodeImpl.getLeftChild();
        Node<E> rightChild = nodeImpl.getRightChild();
        if (leftChild != null && rightChild != null) {
            Node<E> successor = findMin(rightChild);
            nodeImpl.setElement(successor.getElement());
            removeNode(successor);
        } else {
            deleteOneChild(n);
        }
    }

    private void replaceNode(Node<E> n, Node<E> child) {
        NodeImpl<E> parent = validate(parent(n));
        if (parent != null) {
            if (n == parent.getLeftChild()) {
                parent.setLeftChild(child);
            } else {
                parent.setRightChild(child);
            }
        } else {
            setRoot(child);
        }
    }

    private void deleteOneChild(Node<E> n) {
        NodeImpl<E> node = validate(n);
        Node<E> child = node.getRightChild() == null ? node.getLeftChild() : node.getRightChild();
        if (isBlack(n)) {
            if (isRed(child)) {
                makeBlack(child);
            } else {
                deleteCase1(n);
            }
        }
        replaceNode(n, child);
    }

    private void deleteCase1(Node<E> n) {
        if (!isRoot(n)) {
            deleteCase2(n);
        }
    }

    private void deleteCase2(Node<E> n) {
        Node<E> sibling = sibling(n);
        NodeImpl<E> parent = validate(parent(n));
        if (isRed(sibling)) {
            makeRed(parent(n));
            makeBlack(sibling);
            if (n == parent.getLeftChild()) {
                rotate(sibling);
            } else {
                rotate(n);
            }
        }
        deleteCase3(n);
    }

    private void deleteCase3(Node<E> n) {
        NodeImpl<E> sibling = validate(sibling(n));
        if (isBlack(parent(n)) &&
            isBlack(sibling) &&
            isBlack(sibling.getLeftChild()) &&
            isBlack(sibling.getRightChild())) {
            makeRed(sibling);
            deleteCase1(parent(n));
        } else {
            deleteCase4(n);
        }
    }

    private void deleteCase4(Node<E> n) {
        NodeImpl<E> sibling = validate(sibling(n));
        if (isRed(parent(n)) &&
            isBlack(sibling) &&
            isBlack(sibling.getLeftChild()) &&
            isBlack(sibling.getRightChild())) {
            makeRed(sibling);
            makeBlack(parent(n));
        } else {
            deleteCase5(n);
        }
    }

    private void deleteCase5(Node<E> n) {
        NodeImpl<E> parent = validate(parent(n));
        NodeImpl<E> sibling = validate(sibling(n));
        if (isBlack(sibling)) {
            if (parent.getLeftChild() == n &&
                isBlack(sibling.getRightChild()) &&
                isRed(sibling.getLeftChild())) {
                    makeRed(sibling);
                    makeBlack(sibling.getLeftChild());
                    rotate(sibling.getLeftChild());
            } else if (parent.getRightChild() == n &&
                isBlack(sibling.getLeftChild()) &&
                isRed(sibling.getRightChild())) {
                    makeRed(sibling);
                    makeBlack(sibling.getRightChild());
                    rotate(sibling.getRightChild());
            }
        }
        deleteCase6(n);
    }

    private void deleteCase6(Node<E> n) {
        NodeImpl<E> sibling = validate(sibling(n));
        NodeImpl<E> parent = validate(parent(n));
        sibling.setColor(parent.getColor());
        makeBlack(parent);
        if (n == parent.getLeftChild()) {
            makeBlack(sibling.getRightChild());
            rotate(sibling);
        } else {
            makeBlack(sibling.getLeftChild());
            rotate(sibling);
        }
    }

    @Override
    protected void afterElementAdded(Node<E> n) {
        insertCase1(n);
    }

    @Override
    public String toString() {
        return treeString(root(), new StringBuilder()).toString();
    }

    private StringBuilder treeString(Node<E> node, StringBuilder str) {
        if (node == null) {
            return null;
        }
        Node<E> parentNode = parent(node);
        if (right(parentNode) == node && left(parentNode) != null) {
            str.append(",");
        } else if (right(parentNode) == node && left(parentNode) == null) {
            str.append("(null,");
        } else {
            str.append("(");
        }
        str.append(node.getElement());
        NodeImpl<E> nodeImpl = validate(node);
        String firstLetter = nodeImpl.getColor() == Color.BLACK ? "B" : "R";
        str.append(firstLetter);
        for (Node<E> n : children(node)) {
            treeString(n, str);
        }
        if (right(parentNode) == null && parentNode != null) {
            str.append(",null)");
        } else if (node == right(parentNode) || parentNode == null) {
            str.append(")");
        }
        return str;
    }
}
