package com.getjavajob.training.algo1811.golovitskiym.lesson05;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class QueueTest {
    public static void main(String[] args) {
        testAdd();
        testOffer();
        testRemove();
        testPoll();
        testElement();
        testPeek();
    }

    private static void testAdd() {
        Queue<Integer> queue = new ArrayDeque<>();
        for (int i = 0; i < 5; i++) {
            queue.add(i);
        }
        assertEquals("Lesson05.Queue.testAdd", new Object[] {0, 1, 2, 3, 4}, queue.toArray());
    }

    private static void testOffer() {
        Queue<Integer> queue = new ArrayDeque<>();
        for (int i = 0; i < 5; i++) {
            queue.offer(i);
        }
        assertEquals("Lesson05.Queue.testOffer", new Object[] {0, 1, 2, 3, 4}, queue.toArray());
    }

    private static void testRemove() {
        Queue<Integer> queue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        for (int i = 0; i < 3; i++) {
            queue.remove();
        }
        assertEquals("Lesson05.Queue.testRemove", new Object[] {3, 4}, queue.toArray());
    }

    private static void testPoll() {
        Queue<Integer> queue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        for (int i = 0; i < 3; i++) {
            queue.poll();
        }
        assertEquals("Lesson05.Queue.testPoll", new Object[] {3, 4}, queue.toArray());
    }

    private static void testElement() {
        Queue<Integer> queue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson05.Queue.testElement", 0, queue.element());
    }

    private static void testPeek() {
        Queue<Integer> queue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson05.Queue.testPeek", 0, queue.peek());
    }
}
