package com.getjavajob.training.algo1811.golovitskiym.lesson01;

import static com.getjavajob.training.algo1811.golovitskiym.util.DataFromConsole.getIntegerFromConsole;

public class Task07 {
    public static void main(String[] args) {
        System.out.print("Input a number 'n': ");
        int n = getIntegerFromConsole();
        System.out.print("Input a number 'm': ");
        int m = getIntegerFromConsole();
        int[] swappingResult = methodA(n, m);
        System.out.println("A) n after swapping = " + swappingResult[0] + ", m after swapping = " + swappingResult[1]);
        swappingResult = methodB(n, m);
        System.out.println("B) n after swapping = " + swappingResult[0] + ", m after swapping = " + swappingResult[1]);
        swappingResult = methodC(n, m);
        System.out.println("C) n after swapping = " + swappingResult[0] + ", m after swapping = " + swappingResult[1]);
        swappingResult = methodD(n, m);
        System.out.println("D) n after swapping = " + swappingResult[0] + ", m after swapping = " + swappingResult[1]);
    }

    /**
     * Method A swaps 2 variables uses bitwise operators only without additional variable.
     *
     * @param n first variable
     * @param m second variable
     *
     * @return an array, where variables changed there places. Index 0 - new n, index 1 - new m.
     */
    static int[] methodA(int n, int m) {
        if (n != m) {
            n ^= m;
            m ^= n;
            n ^= m;
        }
        return new int[] {n, m};
    }

    /**
     * Method B swaps 2 variables uses bitwise operators only without additional variable.
     *
     * @param n first variable
     * @param m second variable
     *
     * @return an array, where variables changed there places. Index 0 - new n, index 1 - new m.
     */
    static int[] methodB(int n, int m) {
        if (n != m) {
            n = (n & ~m) | (~n & m);
            m = (m & ~n) | (~m & n);
            n = (m & ~n) | (~m & n);
        }
        return new int[] {n, m};
    }

    /**
     * Method C swaps 2 variables uses arithmetical operations only without additional variable.
     *
     * @param n first variable
     * @param m second variable
     *
     * @return an array, where variables changed there places. Index 0 - new n, index 1 - new m.
     */
    static int[] methodC(int n, int m) {
        n += m;
        m = n - m;
        n -= m;
        return new int[] {n, m};
    }

    /**
     * Method D swaps 2 variables uses arithmetical operations only without additional variable.
     *
     * @param n first variable
     * @param m second variable
     *
     * @return an array, where variables changed there places. Index 0 - new n, index 1 - new m.
     */
    static int[] methodD(int n, int m) {
        n *= m;
        m = n / m;
        n /= m;
        return new int[] {n, m};
    }
}
