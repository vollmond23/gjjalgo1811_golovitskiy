package com.getjavajob.training.algo1811.golovitskiym.lesson08.tree.binary.search.balanced;

import com.getjavajob.training.algo1811.golovitskiym.lesson07.tree.Node;
import com.getjavajob.training.algo1811.golovitskiym.lesson08.tree.binary.search.BinarySearchTree;

import java.util.Comparator;

public abstract class AbstractBalanceableTree<E> extends BinarySearchTree<E> {
    AbstractBalanceableTree() {
        super();
    }

    AbstractBalanceableTree(Comparator<E> comparator) {
        super(comparator);
    }

    /**
     * Sets new relationship between parent and child. This method is used by
     * {@link #rotate(com.getjavajob.training.algo1811.golovitskiym.lesson07.tree.Node)} for node and its grandparent,
     * node and its parent, node's child and node's parent relinking.
     *
     * @param parent new parent
     * @param child new child
     * @param makeLeftChild whether new child must be left or right
     */
    private void relink(NodeImpl<E> parent, NodeImpl<E> child, boolean makeLeftChild) {
        NodeImpl<E> grandParent = validate(parent(child));
        if (grandParent != null) {
            if (grandParent.getLeftChild() == child) {
                grandParent.setLeftChild(parent);
            } else if (grandParent.getRightChild() == child) {
                grandParent.setRightChild(parent);
            }
        } else {
            setRoot(parent);
        }
        if (makeLeftChild) {
            if (parent.getLeftChild() != null) {
                Node<E> tempNode = parent.getLeftChild();
                child.setRightChild(tempNode);
            } else {
                child.setRightChild(null);
            }
            parent.setLeftChild(child);
        } else {
            if (parent.getRightChild() != null) {
                Node<E> tempNode = parent.getRightChild();
                child.setLeftChild(tempNode);
            } else {
                child.setLeftChild(null);
            }
            parent.setRightChild(child);
        }
    }

    /**
     * Rotates n with it's parent.
     *
     * @param n node to rotate above its parent
     */
    protected void rotate(Node<E> n) {
        NodeImpl<E> child = validate(n);
        Node<E> parent = parent(n);
        if (parent == null) {
            throw new IllegalArgumentException("Node has not parent");
        }
        NodeImpl<E> parentImpl = validate(parent);
        if (child == parentImpl.getLeftChild()) {
            relink(child, parentImpl, false);
        } else if (child == parentImpl.getRightChild()) {
            relink(child, parentImpl, true);
        }
    }

    /**
     * Performs one rotation of <i>n</i>'s parent node or two rotations of <i>n</i> by the means of
     * {@link #rotate(com.getjavajob.training.algo1811.golovitskiym.lesson07.tree.Node)} to reduce the height of subtree rooted at <i>n1</i>
     *
     * <pre>
     *     n1         n2           n1           n
     *    /          /  \         /            / \
     *   n2    ==>  n   n1  or  n2     ==>   n2   n1
     *  /                         \
     * n                           n
     * </pre>
     *
     * Similarly for subtree with right side children.
     *
     *
     * @param n grand child of subtree root node
     * @return new subtree root
     */
    protected Node<E> reduceSubtreeHeight(Node<E> n) {
        NodeImpl<E> parent = validate(parent(n));
        NodeImpl<E> grandParent = validate(parent(parent));
        if (grandParent == null || parent == null) {
            throw new IllegalArgumentException("Parent or grandparent is null");
        }
        if (grandParent.getLeftChild() == parent) {
            if (parent.getLeftChild() == n) {
                rotate(parent);
                return parent;
            } else {
                rotate(n);
                rotate(n);
                return n;
            }
        } else {
            if (parent.getRightChild() == n) {
                rotate(parent);
                return parent;
            } else {
                rotate(n);
                rotate(n);
                return n;
            }
        }
    }

}
