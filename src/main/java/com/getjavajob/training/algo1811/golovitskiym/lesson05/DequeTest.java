package com.getjavajob.training.algo1811.golovitskiym.lesson05;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class DequeTest {
    public static void main(String[] args) {
        testAddFirst();
        testAddLast();
        testOfferFirst();
        testOfferLast();
        testRemoveFirst();
        testRemoveLast();
        testPollFirst();
        testPollLast();
        testGetFirst();
        testGetLast();
        testPeekFirst();
        testPeekLast();
        testRemoveFirstOccurance();
        testRemoveLastOccurance();
    }

    private static void testAddFirst() {
        Deque<Integer> dequeue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        dequeue.addFirst(23);
        assertEquals("Lesson05.Dequeue.testAddFirst", new Object[] {23, 0, 1, 2, 3, 4}, dequeue.toArray());
    }

    private static void testAddLast() {
        Deque<Integer> dequeue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        dequeue.addLast(23);
        assertEquals("Lesson05.Dequeue.testAddLast", new Object[] {0, 1, 2, 3, 4, 23}, dequeue.toArray());
    }

    private static void testOfferFirst() {
        Deque<Integer> dequeue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        dequeue.offerFirst(23);
        assertEquals("Lesson05.Dequeue.testOfferFirst", new Object[] {23, 0, 1, 2, 3, 4}, dequeue.toArray());
    }

    private static void testOfferLast() {
        Deque<Integer> dequeue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        dequeue.offerLast(23);
        assertEquals("Lesson05.Dequeue.testOfferLast", new Object[] {0, 1, 2, 3, 4, 23}, dequeue.toArray());
    }

    private static void testRemoveFirst() {
        Deque<Integer> dequeue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson05.Dequeue.testRemoveFirst", 0, dequeue.removeFirst());
    }

    private static void testRemoveLast() {
        Deque<Integer> dequeue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson05.Dequeue.testRemoveLast", 4, dequeue.removeLast());
    }

    private static void testPollFirst() {
        Deque<Integer> dequeue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson05.Dequeue.testPollFirst", 0, dequeue.pollFirst());
    }

    private static void testPollLast() {
        Deque<Integer> dequeue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson05.Dequeue.testPollLast", 4, dequeue.pollLast());
    }

    private static void testGetFirst() {
        Deque<Integer> dequeue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson05.Dequeue.testGetFirst", 0, dequeue.getFirst());
    }

    private static void testGetLast() {
        Deque<Integer> dequeue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson05.Dequeue.testGetLast", 4, dequeue.getLast());
    }

    private static void testPeekFirst() {
        Deque<Integer> dequeue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson05.Dequeue.testPeekFirst", 0, dequeue.peekFirst());
    }

    private static void testPeekLast() {
        Deque<Integer> dequeue = new ArrayDeque<>(Arrays.asList(0, 1, 2, 3, 4));
        assertEquals("Lesson05.Dequeue.testPeekLast", 4, dequeue.peekLast());
    }

    private static void testRemoveFirstOccurance() {
        Deque<Integer> dequeue = new ArrayDeque<>(Arrays.asList(0, 23, 2, 23, 4));
        dequeue.removeFirstOccurrence(23);
        assertEquals("Lesson05.Dequeue.testRemoveFirstOccurance", new Object[] {0, 2, 23, 4}, dequeue.toArray());
    }

    private static void testRemoveLastOccurance() {
        Deque<Integer> dequeue = new ArrayDeque<>(Arrays.asList(0, 23, 2, 23, 4));
        dequeue.removeLastOccurrence(23);
        assertEquals("Lesson05.Dequeue.testRemoveLastOccurance", new Object[] {0, 23, 2, 4}, dequeue.toArray());
    }
}
