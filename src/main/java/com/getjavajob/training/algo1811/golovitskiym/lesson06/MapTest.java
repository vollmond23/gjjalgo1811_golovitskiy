package com.getjavajob.training.algo1811.golovitskiym.lesson06;

import java.util.*;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class MapTest {
    private static Map<Integer, String> testMap = new HashMap<>();

    static {
        testMap.put(0, "string0");
        testMap.put(1, "string1");
        testMap.put(2, "string2");
    }

    public static void main(String[] args) {
        testPut();
        testGet();
        testSize();
        testIsEmpty();
        testContainsKey();
        testContainsValue();
        testRemove();
        testPutAll();
        testClear();
        testKeySet();
        testValues();
        testEntrySet();
    }

    private static void testPut() {
        Map<Integer, String> map = new HashMap<>();
        map.put(0, "string0");
        map.put(1, "string1");
        map.put(2, "string2");
        assertEquals("Lesson06.Map.testPut", "{0=string0, 1=string1, 2=string2}", map.toString());
    }

    private static void testGet() {
        Map<Integer, String> map = new HashMap<>(testMap);
        assertEquals("Lesson06.Map.testGet", "string1", map.get(1));
    }

    private static void testSize() {
        Map<Integer, String> map = new HashMap<>(testMap);
        assertEquals("Lesson06.Map.testSize", 3, map.size());
    }

    private static void testIsEmpty() {
        Map<Integer, String> map = new HashMap<>(testMap);
        assertEquals("Lesson06.Map.testIsEmpty", false, map.isEmpty());
    }

    private static void testContainsKey() {
        Map<Integer, String> map = new HashMap<>(testMap);
        assertEquals("Lesson06.Map.testContainsKey", true, map.containsKey(1));
    }

    private static void testContainsValue() {
        Map<Integer, String> map = new HashMap<>(testMap);
        assertEquals("Lesson06.Map.testContainsValue", true, map.containsValue("string1"));
    }

    private static void testRemove() {
        Map<Integer, String> map = new HashMap<>(testMap);
        map.remove(1);
        assertEquals("Lesson06.Map.testRemove", false, map.containsKey(1));
    }

    private static void testPutAll() {
        Map<Integer, String> map = new HashMap<>();
        map.putAll(testMap);
        assertEquals("Lesson06.Map.testPutAll", "{0=string0, 1=string1, 2=string2}", map.toString());
    }

    private static void testClear() {
        Map<Integer, String> map = new HashMap<>(testMap);
        map.clear();
        assertEquals("Lesson06.Map.testClear", true, map.isEmpty());
    }

    private static void testKeySet() {
        Map<Integer, String> map = new HashMap<>(testMap);
        Set<Integer> set = map.keySet();
        assertEquals("Lesson06.Map.testKeySet", new Integer[] {0, 1, 2}, set.toArray());
    }

    private static void testValues() {
        Map<Integer, String> map = new HashMap<>(testMap);
        Collection<String> collection = map.values();
        assertEquals("Lesson06.Map.testValues", new String[] {"string0", "string1", "string2"}, collection.toArray());
    }

    private static void testEntrySet() {
        Map<Integer, String> map = new HashMap<>(testMap);
        Set<Map.Entry<Integer, String>> entrySet = map.entrySet();
        assertEquals("Lesson06.Map.testEntrySet.testIsEmpty", false, entrySet.isEmpty());
        assertEquals("Lesson06.Map.testEntrySet.testEntries", "[0=string0, 1=string1, 2=string2]", Arrays.toString(entrySet.toArray()));
    }
}
