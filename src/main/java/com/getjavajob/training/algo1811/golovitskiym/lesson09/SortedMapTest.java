package com.getjavajob.training.algo1811.golovitskiym.lesson09;

import java.util.*;

import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class SortedMapTest {
    public static void main(String[] args) {
        testComparator();
        testSubMap();
        testHeadMap();
        testTailMap();
        testFirstKey();
        testLastKey();
        testKeySet();
        testValues();
    }

    private static void testComparator() {
        SortedMap<Integer, String> sm = new TreeMap<>();
        sm.put(0, "zero");
        sm.put(1, "one");
        sm.put(2, "two");
        sm.put(3, "three");
        sm.put(4, "four");
        assertEquals("Lesson09.testComparator", true, sm.comparator() == null);
    }

    private static void testSubMap() {
        SortedMap<Integer, String> sm = new TreeMap<>();
        sm.put(0, "zero");
        sm.put(1, "one");
        sm.put(2, "two");
        sm.put(3, "three");
        sm.put(4, "four");
        SortedMap<Integer, String> expectedMap = new TreeMap<>();
        expectedMap.put(1, "one");
        expectedMap.put(2, "two");
        expectedMap.put(3, "three");
        assertEquals("Lesson09.testSubMap", expectedMap.toString(), sm.subMap(1, 4).toString());
    }

    private static void testHeadMap() {
        SortedMap<Integer, String> sm = new TreeMap<>();
        sm.put(0, "zero");
        sm.put(1, "one");
        sm.put(2, "two");
        sm.put(3, "three");
        sm.put(4, "four");
        SortedMap<Integer, String> expectedMap = new TreeMap<>();
        expectedMap.put(0, "zero");
        expectedMap.put(1, "one");
        expectedMap.put(2, "two");
        assertEquals("Lesson09.testHeadMap", expectedMap.toString(), sm.headMap(3).toString());
    }

    private static void testTailMap() {
        SortedMap<Integer, String> sm = new TreeMap<>();
        sm.put(0, "zero");
        sm.put(1, "one");
        sm.put(2, "two");
        sm.put(3, "three");
        sm.put(4, "four");
        SortedMap<Integer, String> expectedMap = new TreeMap<>();
        expectedMap.put(2, "two");
        expectedMap.put(3, "three");
        expectedMap.put(4, "four");
        assertEquals("Lesson09.testTailMap", expectedMap.toString(), sm.tailMap(2).toString());
    }

    private static void testFirstKey() {
        SortedMap<Integer, String> sm = new TreeMap<>();
        sm.put(0, "zero");
        sm.put(1, "one");
        sm.put(2, "two");
        sm.put(3, "three");
        sm.put(4, "four");
        assertEquals("Lesson09.testFirstKey", 0, sm.firstKey());
    }

    private static void testLastKey() {
        SortedMap<Integer, String> sm = new TreeMap<>();
        sm.put(0, "zero");
        sm.put(1, "one");
        sm.put(2, "two");
        sm.put(3, "three");
        sm.put(4, "four");
        assertEquals("Lesson09.testLastKey", 4, sm.lastKey());
    }

    private static void testKeySet() {
        SortedMap<Integer, String> sm = new TreeMap<>();
        sm.put(0, "zero");
        sm.put(1, "one");
        sm.put(2, "two");
        sm.put(3, "three");
        sm.put(4, "four");
        SortedSet<Integer> expectedSet = new TreeSet<>();
        expectedSet.add(0);
        expectedSet.add(1);
        expectedSet.add(2);
        expectedSet.add(3);
        expectedSet.add(4);
        assertEquals("Lesson09.testKeySet", expectedSet.toString(), sm.keySet().toString());
    }

    private static void testValues() {
        SortedMap<Integer, String> sm = new TreeMap<>();
        sm.put(0, "zero");
        sm.put(1, "one");
        sm.put(2, "two");
        sm.put(3, "three");
        sm.put(4, "four");
        List<String> expectedSet = new LinkedList<>();
        expectedSet.add("zero");
        expectedSet.add("one");
        expectedSet.add("two");
        expectedSet.add("three");
        expectedSet.add("four");
        assertEquals("Lesson09.testValues", expectedSet.toString(), sm.values().toString());
    }
}
