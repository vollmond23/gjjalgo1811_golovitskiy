package com.getjavajob.training.algo1811.golovitskiym.lesson01;

import static com.getjavajob.training.algo1811.golovitskiym.lesson01.Task07.*;
import static com.getjavajob.training.algo1811.golovitskiym.util.Assert.assertEquals;

public class Task07Test {
    public static void main(String[] args) {
        testMethodA();
        testMethodB();
        testMethodC();
        testMethodD();
    }

    private static void testMethodA() {
        assertEquals("Lesson01.Task06.testMethodA", new int[] {5, 3}, methodA(3, 5));
    }

    private static void testMethodB() {
        assertEquals("Lesson01.Task06.testMethodB", new int[] {5, 3}, methodB(3, 5));
    }

    private static void testMethodC() {
        assertEquals("Lesson01.Task06.testMethodC", new int[] {5, 3}, methodC(3, 5));
    }

    private static void testMethodD() {
        assertEquals("Lesson01.Task06.testMethodD", new int[] {5, 3}, methodD(3, 5));
    }
}
